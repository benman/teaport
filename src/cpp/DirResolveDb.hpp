#pragma once

#include <nlohmann/json.hpp>
#include "resolve.hpp"

#include "StringCache.hpp"

namespace tea {
    using nlohmann::json;

    class BuildConfig : public std::map<std::string, Version>  {
    public:
        /** scores constraints based on this config.

            @return 0 if no match. > 0 is number of matches.
        */
        int score(const std::vector<BuildConstraint>& constraints) const;
    };



    struct Repo {
        std::string git;
        std::string dir;
        std::string archive;
        std::string script;
    };

    void from_json(const json& js, Repo& repo);
    Package load_spec_json(json package_js, const std::string& filepath);
    Package load_spec_file(const std::string& full_path);
    /** Finds packages simple directory structure.

        struct like so:
            base_dir + "/<package-name>/<version>/<artifact.teaspec>"
        <version> & artifact.teaspec can be anything.
    */
    class DirResolveDb {
    public:
        DirResolveDb(){}
        DirResolveDb(StringCache* scache, std::string base_dir, BuildConfig config={});
        std::vector<Package> scan_dir(const std::string &dir, int max_levels);
        std::vector<Package> operator()(const Dependency &name);

        BuildConfig config;
        /** The directory to scan for projects for */
        std::string base_dir;
        StringCache* string_cache;
    };

    std::vector<Package> score_packages(std::vector<Package> packages, const BuildConfig& config, StringCache& cache);
}