#include "resolve.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <string>

#include "SafePrintf.hpp"
#include "Log.hpp"

using std::vector;
using std::string;
using csd::print;
using csd::str_format;

namespace tea {

    void remove_unused(std::map<std::string, Package>& resolved, std::map<std::string, Dependency>& dependencies_in, const std::vector<Dependency>& mandatory) {
        std::map<std::string, Dependency> dependencies;
        for(Dependency dep : mandatory) {
            dependencies[dep.name] = dependencies_in[dep.name];
        }
        int max_it = resolved.size();
        for(int i = 0; i < max_it; ++i) {
            bool done = true;
            for(auto &pair : resolved) {
                Package& package = pair.second;
                // we don't know about this one yet
                if(!dependencies.count(package.name))
                    continue;
                for(Dependency dep : package.dependencies) {
                    if(dependencies.count(dep.name) == 0) {
                        dependencies[dep.name] = dependencies_in[dep.name];
                        done = false;
                    }
                }
            }
            if(done)
                break;
        }
        std::vector<std::string> remove_list;
        for(auto& pair : dependencies_in) {
            Dependency& dep = pair.second;
            if(!dependencies.count(dep.name))
                remove_list.push_back(dep.name);
        }
        dependencies_in.clear();
        for(std::string& to_remove : remove_list) {
            resolved.erase(to_remove);
        }

        // now we need to add everything back into dependencies_in with new constraints
        // packages that have been removed may have put stricter constraints
        // then necessary so we need to recalculate constraints
        for(Dependency dep : mandatory) {
            dependencies_in[dep.name] = dep;
        }
        for(auto& pair : resolved) {
            Package& package = pair.second;
            for(Dependency dep : package.dependencies) {
                if(dependencies_in.count(dep.name) == 0) {
                    dependencies_in[dep.name] = dep;
                    continue;
                }
                dependencies_in[dep.name].constraint = dependencies_in[dep.name].constraint.limit(dep.constraint);
            }
        }
    }

    Resolve::~Resolve(){}
    std::vector<Package> Resolve::resolve(std::vector<Dependency> dependencies_vector, std::vector<Dependency> locked_versions) {
        if(dependencies_vector.empty())
            return {};
        for(Dependency& locked_dep : locked_versions) {
            for (Dependency& dep : dependencies_vector) {
                if (dep.name == locked_dep.name) {
                    if(!dep.dev_spec_file.empty())
                        continue;
                    VersionConstraint new_constraint = dep.constraint.limit(locked_dep.constraint);
                    if (new_constraint != VersionConstraint{}) {
                        dep.constraint = new_constraint;
                    }
                }
            }
        }

        std::map<std::string, Package> resolved;
        std::map<std::string, Dependency> dependencies;
        std::map<std::string, Dependency> locked_map;

        for(Dependency dep : dependencies_vector)
            dependencies[dep.name] = dep;

        for (Dependency dep : locked_versions) {
            locked_map[dep.name] = dep;
        }
        while(dependencies.size() != resolved.size()) {
            remove_unused(resolved, dependencies, dependencies_vector);
            for(auto &pair : dependencies) {
                Dependency &dependency = pair.second;
                if(resolved.count(dependency.name)) {
                    continue;
                }
                if(dependency.ignore) {
                    Package package;
                    package.name = dependency.name;
                    resolved[package.name] = package;
                    continue;
                }
                std::vector<Package> packages = all_versions(dependency);
                if(packages.empty()) {
                    std::string message = "Package not found ";
                    message += dependency.name;
                    throw NotFoundError(message);
                }

                if (locked_map.count(dependency.name)) {
                    Dependency locked_dep = locked_map[dependency.name];
                    bool lock_can_satisfy = false;
                    for (Package& package : packages) {
                        if (package.score <= 0)
                            continue;
                        if (locked_dep.constraint.check(package.version) &&  dependency.constraint.check(package.version)) {
                            lock_can_satisfy = true;
                            break;
                        }
                    }
                    if (lock_can_satisfy) {
                        for (Package& package : packages) {
                            if (package.score <= 0)
                                continue;
                            if (!locked_dep.constraint.check(package.version)) {
                                package.score = 0;
                            }
                        }
                    }
                }

                Package package;

                for(std::size_t i = 0; i < packages.size(); ++i) {
                    if(packages[i].score <= 0)
                        continue;
                    if(!packages[i].dev && !dependency.constraint.check(packages[i].version)) {
                        continue;
                    }
                    package = packages[i];
                    break;
                }
                if(package.name.empty()) {
                    // TODO: print version constraint
                    log_message("F1002", csd::str_format("could not resolve: {}/{}", dependency.name, to_string(dependency.constraint)));
                }
                if(package.name != dependency.name) {
                    log_message("F1003", csd::str_format("{} != {}", package.name, dependency.name));
                }
                resolved[dependency.name] = package;
                for(Dependency dep : package.dependencies) {
                    bool updated = false;
                    if(dependencies.count(dep.name)) {
                        if(dependencies[dep.name].ignore) {
                            continue;
                        }
                        Dependency dep_resolved = dependencies[dep.name];
                        VersionConstraint constraint = dependencies[dep.name].constraint.limit(dep.constraint);
                        if(dep_resolved.force) {
                            constraint = dep_resolved.constraint;
                        }
                        if(constraint != dependencies[dep.name].constraint) {
                            if(constraint.match == Match::none) {
                                log_message("E1002", str_format("{} wants {}/{}", dependency.name, dep.name, dep.constraint));
                                print("    another package requested {}", dependencies[dep.name].constraint);
                                break;
                            }
                            dependencies[dep.name].constraint = constraint;
                            updated = true;
                        }
                    } else {
                        dependencies[dep.name] = dep;
                        updated = true;
                    }
                    // this dependency needs to be resolved again
                    // because it's version constraint is changed
                    // so we need to try again.
                    if(updated) {
                        resolved.erase(dep.name);
                    }

                }

            }
        }
        std::vector<Package> result;
        for(auto &pair : resolved) {
            if(dependencies[pair.first].ignore)
                continue;
            result.push_back(pair.second);
        }
        return result;
    }

    std::vector<Package> Resolve::all_versions(const Dependency &package_name) {
        return {};
    }


    std::vector<Package> CompoundResolve::all_versions(const Dependency &dependency) {
        std::vector<Package> result;
        for(auto func : mPackageFinders) {
            std::vector<Package> list = func(dependency);
            result.insert(result.end(), list.begin(), list.end());
        }
        std::sort(result.begin(), result.end(), [](const Package &a, const Package &b) -> bool {
            if(a.version != b.version) {
                return a.version > b.version;
            }
            return a.score > b.score;
        });
        return result;
    }

    void CompoundResolve::add_finder(const std::function<std::vector<Package>(const Dependency&)> &func) {
        mPackageFinders.push_back(func);
    }
}