#pragma once

#include "Sqlite3.hpp"
#include "DirCache.hpp"
#include "ProjectSpec.hpp"
#include "StringCache.hpp"

namespace tea {
    // implemented in main_install.cpp

    class TeaspecInstaller {
    public:
        TeaspecInstaller(ProjectSpec& spec, bool update=false);
        void load_lock_file();
        void resolve();
        int install();

        std::string artifact_cache_dir;

        Sqlite3 database;
        DirCache cache;
        StringCache string_cache;
        CompoundResolve resolver;
        CompoundArtifactLoader artifactLoader;
        ProjectSpec spec;

        std::vector<Dependency> locked_versions;

        std::vector<Package> resolved;
        bool using_lock = false;
        bool update = false;
    };
}