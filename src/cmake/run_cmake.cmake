function(cmake_configure project build_dir)
    set(args "-DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/install"
        -DCMAKE_INSTALL_RPATH=""
        -DCMAKE_SKIP_RPATH=ON
    )
    if(CMAKE_TOOLCHAIN_FILE)
        list(APPEND args -DCMAKE_TOOLCHAIL_FILE="${CMAKE_TOOLCHAIN_FILE}")
    endif()
    find_program(NINJA_COMMAND ninja)
    if(NINJA_COMMAND)
        list(APPEND args -GNinja)
    else()
        list(APPEND args -G"Unix Makefiles")
    endif()
    if(NOT EXISTS "${build_dir}")
        file(MAKE_DIRECTORY "${build_dir}")
    endif()

    if(MSVC)
        list(APPEND -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})
    else()
        list(APPEND -DCMAKE_BUILD_TYPE=Release)
    endif()
    message(STATUS "Configuring ${project}")
    execute_process(COMMAND ${CMAKE_COMMAND} ${args} "${project}" ${ARGN}
        WORKING_DIRECTORY "${build_dir}"
        RESULTS_VARIABLE result
    )
    if(NOT "${result}" STREQUAL "0")
        message(FATAL_ERROR "could not configure")
    endif()
endfunction()


function(cmake_build dir)
    set(args --build "${dir}")
    execute_process(COMMAND ${CMAKE_COMMAND} ${args} ${ARGN}
        RESULTS_VARIABLE result
    )
    if(NOT "${result}" STREQUAL "0")
        message(FATAL_ERROR "could not build ${dir}")
    endif()
endfunction()