#include <cmath>

#include "Sqlite3.hpp"
#include "fileutils.hpp"
#include "tea_dir.hpp"
#include "shell.hpp"
#include <nlohmann/json.hpp>
#include "resolve.hpp"
#include "DirCache.hpp"
#include "Log.hpp"


using namespace tea;
static int print_help() {
const char* str=
R"(teaport status [apps...]
    show versions of some commands installed on the system
)";
    printf("%s", str);
    return 1;
}


namespace {
    struct Command {
        std::string name;
        StrictVersion version;
        bool found = false;
        void init(std::string command) {
            try {
                name = command;
                version = command_version(command);
                found = true;
            } catch (FileNotFoundError& e) {
                found = false;
            } catch (CommandError& e) {
                version = {};
                found = true;
            }
        }
    };


    std::string pad_right(std::string input, std::size_t size) {
        while (input.size() < size) {
            input += ' ';
        }
        return input;
    }
    struct Table {
        typedef std::vector<std::string> Row;
        void print_cli() {
            std::vector<int> sizes;
            for (std::size_t i = 0; i < mHeader.size(); ++i) {
                sizes.push_back(mHeader[i].size()+1);
            }
            for (Row& row : mRows) {
                for (std::size_t i = 0; i < row.size(); ++i) {
                    sizes[i] = std::max<int>(sizes[i], row[i].size()+1);
                }
            }

            for (int& size : sizes) {
                int new_size = size/4;
                new_size *= 4;
                if (new_size < size)
                    new_size += 4;
                size = new_size;
            }

            for (std::size_t i = 0; i < mHeader.size(); ++i) {
                std::cout << pad_right(mHeader[i], sizes[i]);
            }
            std::cout << "\n";
            int columns = mHeader.size();
            for (Row& row : mRows) {
                for (int i = 0; i < columns; ++i) {
                    std::cout << pad_right(row[i], sizes[i]);
                }
                std::cout << '\n';
            }
        }
        Row mHeader;
        std::vector<Row> mRows;
    };
}
int main_status(std::vector<std::string> args) {
    for(std::string& arg : args) {
        if(arg == "--help") {
            print_help();
            return 1;
        }
    }

    args.erase(args.begin());

    std::vector<Command> applications;
    std::vector<std::string> names {"zip", "unzip", "git", "git-lfs", "curl",
        "scp", "sftp", "ssh"};

    for (std::string app : args) {
        if (app[0] != '-')
            names.push_back(app);
    }

    std::sort(names.begin(), names.end());
    names.erase(std::unique(names.begin(), names.end()), names.end());
    for (std::string name : names) {
        Command command;
        command.name = name;
        command.init(name);
        applications.push_back(command);
    }

    Table table;

    table.mHeader = std::vector<std::string>{"Name", "Version"};

    for (Command& command : applications) {
        Table::Row row;
        row.push_back(command.name);
        if (command.found)
            row.push_back(to_string(command.version));
        else
            row.push_back("Not Found");
        table.mRows.push_back(row);
    }

    table.print_cli();
    return 0;
}