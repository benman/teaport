#include "shell.hpp"
#include <regex>
#include "test_config.h"
#include "SafePrintf.hpp"
#include "Log.hpp"
#include "Version.hpp"

#include <cxxtest/TestSuite.h>

using namespace tea;

class VersionSuite : public CxxTest::TestSuite {
public:
    void testBasics() {
        ErrorCode code("V0002");
        if(code.code_int() != 2) {
            csd::stdout_format("invalid int {} != 2\n", code.code_int());
            TS_ASSERT(false);
        }
        if(!code.is_verbose()) {
            csd::stdout_format("error code V0002 must be verbose\n");
            TS_ASSERT(false);
        }
        CompletedProcess process = tea::system_capture_checked({TEAPORT_EXE, "--version"});
        TS_ASSERT_EQUALS(process.exit_code, 0);
        process.stdout_data.push_back(0);
        std::string str = reinterpret_cast<char*>(&process.stdout_data[0]);
        #ifdef _WIN32
        std::regex regex("teaport version \\d+\\.\\d+\\.\\d+\r\n");
        #else
        std::regex regex("teaport version \\d+\\.\\d+\\.\\d+\n");
        #endif
        bool match = std::regex_match(str, regex);
        if(!match) {
            csd::stdout_format("version didn't match regex\n");
            TS_ASSERT(false);
        }

        tea::Version a("a");
        tea::Version b("b");
        if (to_string(a) != "a") {
            csd::print("to_string(a) != \"a\"");
            TS_ASSERT(false);
        }
        if(!(a < b)) {
            csd::print("a >= b");
            TS_ASSERT(false);
        }
        a = tea::Version("1.0");
        if(!(b < a)) {
            csd::print("b >= 1.0");
            TS_ASSERT(false);
        }
        if(a == b || !(a != b)) {
            csd::print("a == 1.0");
            TS_ASSERT(false);
        }

    }

    void testVersionLimits() {
        tea::VersionConstraint v1("1.0.1");
        tea::VersionConstraint v2("1.0.2");
        tea::VersionConstraint v3 = v1.limit(v2);

        TS_ASSERT_EQUALS(v3, tea::VersionConstraint{});
    }
    void testTrollConstructors() {
        // don't crash trying to parse these
        csd::print("doing some parse tests");
        csd::print("    latest.version");
        Version v("latest.version");
        csd::print("    gibberish");
        Version v2("uhcr.ch3.2church-abont*32(3234");
        csd::print("    '..'");
        Version v3("    ...");
        csd::print("    ,,3");
        Version v4(",,3");
        // this is an invalid version
        csd::print("    .3");
        Version v5(".3");

    }

    void testConstraint() {
        tea::VersionConstraint v1("3.6.1");
        tea::Version v2("3.7.3");
        TS_ASSERT_EQUALS(v1.check(v2), false);

        v1 = tea::VersionConstraint("1.0.1");
        v2 = "1.0.1";
        TS_ASSERT_EQUALS(v1.check(v2), true);
    }

    void testCommandParsing() {
        std::string gcc = R"(gcc (Ubuntu 9.2.1-9ubuntu2) 9.2.1 20191008
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
)";

        std::string zip_linux = R"(Copyright (c) 1990-2008 Info-ZIP - Type 'zip "-L"' for software license.
This is Zip 3.0 (July 5th 2008), by Info-ZIP.
Currently maintained by E. Gordon.  Please send bug reports to
the authors using the web page at www.info-zip.org; see README for details.

Latest sources and executables are at ftp://ftp.info-zip.org/pub/infozip,
as of above date; see http://www.info-zip.org/ for other sites.

Compiled with gcc 6.3.0 20170415 for Unix (Linux ELF).

Zip special compilation options:
        USE_EF_UT_TIME       (store Universal Time)
        BZIP2_SUPPORT        (bzip2 library version 1.0.6, 6-Sept-2010)
            bzip2 code and library copyright (c) Julian R Seward
            (See the bzip2 license for terms of use)
        SYMLINK_SUPPORT      (symbolic links supported)
        LARGE_FILE_SUPPORT   (can read and write large files on file system)
        ZIP64_SUPPORT        (use Zip64 to store large files in archives)
        UNICODE_SUPPORT      (store and read UTF-8 Unicode paths)
        STORE_UNIX_UIDs_GIDs (store UID/GID sizes/values using new extra field)
        UIDGID_NOT_16BIT     (old Unix 16-bit UID/GID extra field not used)
        [encryption, version 2.91 of 05 Jan 2007] (modified for Zip 3)

Encryption notice:
        The encryption code of this program is not copyrighted and is
        put in the public domain.  It was originally written in Europe
        and, to the best of our knowledge, can be freely distributed
        in both source and object forms from any country, including
        the USA under License Exception TSU of the U.S. Export
        Administration Regulations (section 740.13(e)) of 6 June 2002.

Zip environment options:
             ZIP:  [none]
          ZIPOPT:  [none]
)";

        std::string unzip_linux = R"(UnZip 6.00 of 20 April 2009, by Debian. Original by Info-ZIP.

Latest sources and executables are at ftp://ftp.info-zip.org/pub/infozip/ ;
see ftp://ftp.info-zip.org/pub/infozip/UnZip.html for other sites.

Compiled with gcc 9.2.0 for Unix (Linux ELF).

UnZip special compilation options:
        ACORN_FTYPE_NFS
        COPYRIGHT_CLEAN (PKZIP 0.9x unreducing method not supported)
        SET_DIR_ATTRIB
        SYMLINKS (symbolic links supported, if RTL and file system permit)
        TIMESTAMP
        UNIXBACKUP
        USE_EF_UT_TIME
        USE_UNSHRINK (PKZIP/Zip 1.x unshrinking method supported)
        USE_DEFLATE64 (PKZIP 4.x Deflate64(tm) supported)
        UNICODE_SUPPORT [wide-chars, char coding: UTF-8] (handle UTF-8 paths)
        LARGE_FILE_SUPPORT (large files over 2 GiB supported)
        ZIP64_SUPPORT (archives using Zip64 for large files supported)
        USE_BZIP2 (PKZIP 4.6+, using bzip2 lib version 1.0.6, 6-Sept-2010)
        VMS_TEXT_CONV
        WILD_STOP_AT_DIR
        [decryption, version 2.11 of 05 Jan 2007]

UnZip and ZipInfo environment options:
           UNZIP:  [none]
        UNZIPOPT:  [none]
         ZIPINFO:  [none]
      ZIPINFOOPT:  [none]
)";

        std::string git = "git version 2.20.1";
        std::string tar_gnu = R"(tar (GNU tar) 1.30
Copyright © 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Écrit par John Gilmore et Jay Fenlason.
)";
        std::string bash = R"(GNU bash, version 5.0.3(1)-release (x86_64-pc-linux-gnu)
Copyright (C) 2019 Free Software Foundation, Inc.
Licence GPLv3+ : GNU GPL version 3 ou ultérieure <http://gnu.org/licenses/gpl.html>

Ceci est un logiciel libre ; vous être libre de le modifier et de le redistribuer.
AUCUNE GARANTIE n'est fournie, dans les limites permises par la loi.
)";

        std::string cmake = R"(cmake version 3.13.4

CMake suite maintained and supported by Kitware (kitware.com/cmake).
)";

        std::string ssh = "OpenSSH_8.0p1 Ubuntu-6build1, OpenSSL 1.1.1c  28 May 2019";
        std::string date_dot = "something 2019. version 12.2";
        auto from_output = [] (const std::string& text) -> std::string {
            return to_string(StrictVersion::from_output(text));
        };
        TS_ASSERT_EQUALS(StrictVersion::from_output(gcc), StrictVersion(9, 2, 1));
        TS_ASSERT_EQUALS(from_output(zip_linux), "3.0.0");
        TS_ASSERT_EQUALS(from_output(unzip_linux), "6.0.0");
        TS_ASSERT_EQUALS(from_output(git), "2.20.1");
        TS_ASSERT_EQUALS(from_output(tar_gnu), "1.30.0");
        TS_ASSERT_EQUALS(from_output(bash), "5.0.3");
        TS_ASSERT_EQUALS(from_output(cmake), "3.13.4");
        TS_ASSERT_EQUALS(from_output(ssh), "8.0.0");
        TS_ASSERT_EQUALS(from_output(date_dot), "12.2.0");
    }
};


