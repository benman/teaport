#include "SpecLockFile.hpp"

#include <map>

#include "container_ops.hpp"
#include "fileutils.hpp"

namespace tea {

    json to_json_lock(const std::vector<Package>& resolved) {
        json result;
        json dependencies;
        std::vector<std::string> order;
        std::map<std::string, Package> packages;
        for(const Package& package : resolved) {
            json dep;
            dep["version"] = to_string(package.version);
            dep["score"] = package.score;
            dependencies[package.name] = dep;
            packages[package.name] = package;
        }


        std::map<std::string, int> added;
        std::vector<const Package*> queue;
        for (const Package& package : resolved) {
            queue.push_back(&package);
        }
        while (queue.size() > 0) {
            for (std::size_t i = 0; i < queue.size(); ++i) {
                const Package* package = queue[i];

                bool ready = true;
                for (const Dependency& dep : package->dependencies) {
                    if (!added.count(dep.name)) {
                        ready = false;
                        break;
                    }
                }
                if (ready) {
                    added[package->name] = 1;
                    order.push_back(package->name);
                    queue.erase(queue.begin() + i);
                    --i;
                }
            }
        }
        result["dependencies"] = dependencies;
        result["order"] = [order]() {
            json js;
            for (auto& name : order) {
                js.push_back(name);
            }
            return js;
        }();
        result["version_code"] = 2;
        return result;
    }


    SpecLockFile load_lock_file_v1(const json& resolved_js) {
        SpecLockFile lockfile;

        for(const json& dep_js : resolved_js["dependencies"]) {
            Dependency dep;
            dep.name = dep_js["name"];
            std::string version = dep_js["version"];
            dep.constraint = VersionConstraint(version);
            lockfile.dependencies.push_back(dep);
        }

        return lockfile;
    }

    SpecLockFile load_lock_file_v2(const json& js) {
        SpecLockFile lockfile;
        for(auto it = js["dependencies"].cbegin(); it != js["dependencies"].cend(); ++it) {
            Dependency dep;
            dep.name = it.key();
            json dep_js = it.value();
            std::string version = dep_js["version"];
            dep.constraint = VersionConstraint(version);
            lockfile.dependencies.push_back(dep);
        }
        return lockfile;
    }
    SpecLockFile load_lock_file(std::string lock_file) {
        json resolved_js = load_json_file(lock_file);
        SpecLockFile speclockfile;
        int version_code = resolved_js.contains("version_code")? resolved_js["version_code"].get<int>() : 1;
        speclockfile = [version_code, resolved_js]() -> SpecLockFile {
            if (version_code == 1) {
                return load_lock_file_v1(resolved_js);
            } else if (version_code == 2) {
                return load_lock_file_v2(resolved_js);
            }
            return SpecLockFile{};
        }();
        // having it sorted makes comparisons easier
        sort_list(speclockfile.dependencies, [] (const Dependency& a, const Dependency& b) -> bool {
            return a.name < b.name;
        });
        return speclockfile;
    }
}