@page types Types

# ProjectSpec

<table>
    <tr><td>repos</td><td>Array of repos</td></tr>
    <tr><td>dependencies</td><td>Dictionary of dependencie names to version as string or
        object describing dependency more in detail.</td></tr>
    <tr><td>build_config</td><td>dictionary of build config, each name maps to version</td></tr>
    <tr><td>output_dir</td><td>Directory to put all dependencies into. This
        directory will be managed by teaport and unrelated files will be deleted.
        Alternative to specifying `--output-dir` via command line.</td></tr>
</table>

# Repo

<table><tr><td>git</td><td>Url to a git repository</td></tr>
    <tr><td>dir</td><td>path, can be relative to spec file to a directory
        that has library specs. Directory must be structured as a repository</td></tr>
</table>

# Dependency

<table><tr><td>version</td><td>Version number as string.</td></tr>
    <tr><td>force</td><td>boolean. If true this specific version is forced
        any conflicts are ignored</td></tr>
    <tr><td>ignore</td><td>boolean. If true this dependency is ignored and will
        not be resolved</td></tr>
</table>

# Library Spec

```
{
    "version": Version,
    "name": package_name,
    "artifact": Artifact,
    "constraints": [TargetConstraint],
    "dependencies": {package_name: Dependency | Version},
    "dir_map": {from_dir -> to_dir: string},
    "file_map": {file_regex -> FileMap | string},
    "exclude_files": [exclude_regex]
}
```

<table>
    <tr><td>name</td><td>Name of library. can only include a-z, A-Z, 0-9 and _.
        Any other characters aren't permitted, and name must start with a letter</td></tr>
    <tr><td>version</td><td>Version of the library. Preferably to use semantic versioning.
        if more than 3 levels is used, or non-numbers are used then an exact match is
        required when depending on this library.</td></tr>
    <tr><td>artifact</td><td>Details about where to download and find the package</td></tr>
    <tr><td>dependencies</td><td>Dictionary of dependencie names to version as string.
        Object file like in ProjectSpec is not permitted.</td></tr>
    <tr><td>constraints</td><td>array of TargetConstraint. build_config in ProjectSpec
        must pass all constraints. If there are multiple constraints of the same name
        then 1 constraint must pass of given name</td></tr>
    <tr><td>file_map</td><td>A mapping of a regular expression for src files to destination
        directory. files matching items in exclude_files will not be mapped</td></tr>
    <tr><td>dir_map</td><td>translates to a file_map of `"src/*/.*":"destination"`.
        `exnclude_files` will be ignored for entries in dir_map.</td></tr>
    <tr><td>exclude_files</td><td>array of regexes to match for files to be excluded</td></tr>
</table>

# TargetConstraint

<table><tr><td>name</td><td>name of constraint to match against in build_config. Must
    conform to variable names: start with a letter, numbers and _ allowed.</td></tr>
    <tr><td>=</td><td>Specify a version to match exactly. You can do `3.24+` to match
        with semantic versioning. If you specify this other parameters are ignored.</td></tr>
    <tr><td>&gt;=</td><td>sets the minimum version. If you set to `3.2` this will match
        `5.3` with no maximum.</td></tr>
    <tr><td>&lt;</td><td>Maximum version to match to</td></tr>
    <tr><td>&lt;=</td><td>Not supported and will be ignored</td></tr>
</table>

# Artifact

<table>
    <tr><td>git</td><td>The git url.</td></tr>
    <tr><td>tag</td><td>The branch name or tag name to be used.</td></tr>
    <tr><td>archive</td><td>http url to a file, or a location locally
        to a file or directory.</td></tr>
    <tr><td>extract</td><td>by default is archive points to a zip file it will
        extract it. Set this to false to prevent this.</td></tr>
    <tr><td>remove_nests</td><td>default: false. Set to true if dir_map and file_map
        to be relative to the subdirectory with more than 1 entry. Usefull when
        zip archives have nests with a folder name that's not predictable.</td></tr>
</table>