#include <nlohmann/json.hpp>
#include <iostream>

int main() {
    nlohmann::json hello;
    hello["ok"] = "world";

    std::cout << hello.dump(4) << '\n';
}