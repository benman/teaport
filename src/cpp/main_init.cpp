#include <string>
#include <nlohmann/json.hpp>

#include "fileutils.hpp"


using nlohmann::json;

static void print_project() {
const char* str = R"({
    "repos": [

    ],
    "dependencies":{
    },
    "build_config": {

    }
}
)";
    printf("%s", str);
}


static void print_lib() {
const char* str = R"({
    "name": "MyLibrary",
    "version": "1.0.0",
    "summary": "brief description",
    "description": "long description",
    "homepage": "https://url",
    "license": {
        "type": "MIT",
        "text": ""
    },
    "authors": {
        "Your Name": "yourl@email.com"
    },
    "artifact": {
        "archive": "zipFile.zip",
        "git": "giturl",
        "tag": "1.0.0"
    },
    "constraints": [
    ],
    "dependencies": {
    },
    "dir_map": {
    },
    "file_map": {
    },
    "exclude_files": [
    ]
}
)";
    printf("%s", str);
}

int main_init(std::vector<std::string> args) {
    args.erase(args.begin());
    if(args.size() == 0 || args[0] == "project") {
        print_project();
        return 0;
    }
    if(args[0] == "lib") {
        print_lib();
        return 0;
    }
    return 1;
}