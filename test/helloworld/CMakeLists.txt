cmake_minimum_required(VERSION 3.6)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Release/Debug")
endif()

project(helloworld VERSION 1.0.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(NOT MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++17")
endif()




add_executable(helloworld helloworld.cpp)

target_include_directories(helloworld PUBLIC
    dependencies/nlohmann_json
)
if(MSVC)
    target_compile_options(helloworld PUBLIC -Zc:__cplusplus)
endif()

