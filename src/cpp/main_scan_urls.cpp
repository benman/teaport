#include <string>
#include <nlohmann/json.hpp>

#include "TeaspecInstaller.hpp"
#include "fileutils.hpp"
#include "lint.hpp"
#include "SafePrintf.hpp"
#include "TablePrinter.hpp"
#include "utils/link_scanner.hpp"

using nlohmann::json;
using namespace tea;
using namespace csd;

static int print_help(){
const char* str =
R"(
    scan_urls <url>
        scrape url for urls.

)";

    printf("%s", str);
    return 1;
}

struct Row {
    std::string package_name;
    Version     current_version;
    Version     restricted_version;
    Version     latest_version;
};

// TODO: needs a test
int main_scan_urls(std::vector<std::string> args) {
    args.erase(args.begin());
    if(args.size() < 1)
        return print_help();
    bool output_json = false;
    std::string url;
    for(std::string& arg : args) {
        if(arg == "--help") {
            return print_help();
        }
        if (arg == "--json")
            output_json = true;
        if (arg[0] != '-')
            url = arg;
    }
    if (url.empty())
        return print_help();

    auto urls = scan_links(url);
    for (auto& url : urls) {
        std::cout << url << '\n';
    }

    // todo json output
    return 0;
}