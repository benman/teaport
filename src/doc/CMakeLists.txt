find_program(doxygen doxygen)
configure_file(Doxyfile.in ${CMAKE_BINARY_DIR}/Doxyfile)

if(doxygen)
    file(GLOB_RECURSE doc_files
        pages/*.md
        Doxyfile.in)
    message(STATUS "doc files = ${doc_files}")
    add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/doc/html/index.html
        COMMAND doxygen ${CMAKE_BINARY_DIR}/Doxyfile
        DEPENDS ${doc_files}
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    )

    add_custom_target(documentation ALL DEPENDS ${CMAKE_BINARY_DIR}/doc/html/index.html)
endif()

