#include "lint.hpp"
#include <regex>
#include "DirGlob.hpp"
#include "SafePrintf.hpp"
#include "DirResolveDb.hpp"
#include "link_scanner.hpp"
#include "stringutils.hpp"

using nlohmann::json;

namespace tea {
    void lint_name(const std::string& name) {
        if(name.size() == 0) {
            throw LintError("name is nothing, give it a name");
        }
        if(std::isdigit(name[0])) {
            throw LintError("name cannot start with a number");
        }
        if(name[0] == '-')
            throw LintError("name cannot start with a '-'");
        if(name[0] == '_')
            throw LintError("name cannot start with '_'");

        for(std::size_t i = 0; i < name.size(); ++i) {
            if(name[i] >= 'a' && name[i] <= 'z')
                continue;
            if(name[i] >= 'A' && name[i] <= 'Z')
                continue;
            if(name[i] == '_')
                continue;
            if(std::isdigit(name[i]))
                continue;
            std::string message = "name cannot have '";
            message += name[i];
            message += "' characters";
            throw LintError(message);
        }
        // it passes
    }

    void lint_version(const std::string& version) {
        if(version.size() == 0) {
            throw LintError("you must specify a version");
        }
        if(version == "ignore")
            throw LintError("'ignore' is reserved and cannot be used as a version");

        if(version[0] == '-')
            throw LintError("version cannot start with a '-'");

        for(std::size_t i = 0; i < version.size(); ++i) {
            if(version[i] >= 'a' && version[i] <= 'z')
                continue;
            if(version[i] >= 'A' && version[i] <= 'Z')
                continue;
            if(version[i] == '_')
                continue;
            if(std::isdigit(version[i]))
                continue;
            if(version[i] == '-' || version[i] == '.')
                continue;
            std::string message = "version cannot have '";
            message += version[i];
            message += "' characters";
            throw LintError(message);
        }
    }
    void lint_lib_spec(const json& spec) {
        lint_name(spec["name"]);
        std::string version = spec["version"];
        if (starts_with(version, "scan-http://") || starts_with(version, "scan-https://")) {
            std::string url = version.substr(5);
            auto links = scan_links(version.substr(5));
            if (links.empty()) {
                throw LintError("no links found at " + url);
            }
        } else {
            lint_version(spec["version"]);
        }
        if(spec.count("file_map")) {
            for(json::const_iterator it = spec["file_map"].cbegin(); it != spec["file_map"].cend(); ++it) {
                DirGlobExpression expression;

                try {
                    expression = it.key();
                } catch(std::exception& e) {
                    csd::print("error: invalid regex {}", it.key());
                    throw;
                }
            }
        }

        // make sure it loads
        Package package = spec;
    }
}