#include <fstream>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

#include "fileutils.hpp"
#include "SafePrintf.hpp"
#include "shell.hpp"
#include "Sqlite3.hpp"
#include "tea_dir.hpp"
#include "lint.hpp"
#include "Log.hpp"
#include "git.hpp"
#include "stringutils.hpp"

using namespace tea;
using csd::print;
using nlohmann::json;

static int print_help() {
const char* str =
R"(
    publish <repo> <spec>
        publishes to repo. You must add it first with teaport repo add

)";

    printf("%s", str);
    return 1;
}

int main_publish(std::vector<std::string> args) {
    if(args.size() < 3)
        return print_help();
    bool force = false;
    args.erase(args.begin());
    std::string name;
    std::string spec;
    for(std::string& arg : args) {
        if(arg == "--help") {
            return print_help();
        }
        if(arg == "-f") {
            force = true;
            continue;
        }
        if(name.empty()) {
            name = arg;
            continue;
        }
        if(spec.empty()) {
            spec = arg;
            continue;
        }
    }
    if(name.empty()) {
        log_message("E1007", "you must specify repo name");
        return print_help();
    }
    if(spec.empty()) {
        log_message("E1008", "you must specify spec file");
        return print_help();
    }
    json js = load_json_file(spec);
    lint_lib_spec(js);

    Sqlite3 database = init_db();
    Sqlite3Statement statement = database.prepare("SELECT path, url FROM repos WHERE name=?");
    statement.bind_text(1, name);
    if(statement.step() != SQLITE_ROW) {
        print("no repo named {}", name);
    }
    std::string path = statement.column_text(0);
    std::string url = statement.column_text(1);

    if(!is_dir(path)) {
        int result = tea::system({"git", "clone", "--depth", "1", "--single-branch", url, path});
        if(result != 0) {
            print("failed");
            return 1;
        }
    } else {
        bool result = git_make_latest(path, url);
        if(!result) {
            print("failed");
            return 1;
        }
    }

    name = js["name"];
    std::string version = js["version"];
    std::string subdir = version;
    if (starts_with(version, "scan-http://") || starts_with(version, "scan-https://")) {
        subdir = "scan";
    }
    std::string output = path + "/" + name + "/";
    tea::mkdir(output);
    output += subdir;
    tea::mkdir(output);
    output += "/";
    output += basename(spec);

    if(force) {
        tea::rmdir(output);
    }
    if(path_exists(output)) {
        log_message("E1009", csd::str_format("{}/{}/{} already exists", name, subdir, basename(spec)));
        return 1;
    }
    copy_file(spec, output);
    tea::system({"git", "-C", path, "add", output});
    tea::system({"git", "-C", path, "commit", "-m", output.substr(path.size()) + " updated"});
    tea::system({"git", "-C", path, "push"});
    return 0;
}