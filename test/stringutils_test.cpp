#include "shell.hpp"
#include <regex>
#include <nlohmann/json.hpp>

#include "test_config.h"
#include "stringutils.hpp"

using namespace tea;

bool is_equal(const CommandLine& a, const CommandLine& b) {
    if (a.size() != b.size())
        return false;
    for (std::size_t i = 0; i < a.size(); ++i) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}

class StringUtilsSuite : public CxxTest::TestSuite {
public:
    void testReplace() {
        std::map<std::string, std::string> vars;
        vars["hello"] = "world";
        vars["yes"] = "true";
        std::string world = replace_string_variables("$hello", vars);
        TS_ASSERT_EQUALS(world, "world");

        std::string hw = replace_string_variables("hello $hello, it$'s great", vars);
        TS_ASSERT_EQUALS(hw, "hello world, it's great");

        std::string tt = replace_string_variables("yay $$\\$hello $'yes $yes", vars);
        TS_ASSERT_EQUALS(tt, "yay $hello 'yes true");


        tt = replace_string_variables("yay $yes $", vars);
        TS_ASSERT_EQUALS(tt, "yay true ");

        tt = replace_string_variables("\\", vars);
        TS_ASSERT_EQUALS(tt, "\\");

        tt = replace_string_variables("\\hello world", vars);
        TS_ASSERT_EQUALS(tt, "\\hello world");
    }
};


