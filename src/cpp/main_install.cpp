#include "TeaspecInstaller.hpp"
#include "DirResolveDb.hpp"

#include <nlohmann/json.hpp>
#include <fstream>

#include "container_ops.hpp"
#include "DirCache.hpp"
#include "DirGlob.hpp"
#include "exceptions.hpp"
#include "fileutils.hpp"
#include "Log.hpp"
#include "ProjectSpec.hpp"
#include "SafePrintf.hpp"
#include "ScriptResolve.hpp"
#include "shell.hpp"
#include "SpecLockFile.hpp"
#include "SpecResolverDb.hpp"
#include "Sqlite3.hpp"
#include "stringutils.hpp"
#include "tea_dir.hpp"
#include "StringCache.hpp"

using namespace tea;
using namespace csd;





std::vector<std::string> resolve_args(const std::string& file) {
    std::vector<std::string> args;


    return args;
}

static int print_help() {
const char* str =
R"(
    install <spec> --output-dir <output>
        reads spec and outputs dependencies to <output> dir.
)";

    printf("%s", str);

    return 1;
}
class Args {
public:
    Args(){}
    Args(int argc, const char** argv);
    void load(std::vector<std::string> args);
    
    explicit operator bool() const {
        return success;
    }

    ProjectSpec spec;
    std::string spec_file;

    bool success = false;
    bool update = false;

    bool first = true;
};

Args::Args(int argc, const char** argv) {
    std::vector<std::string> args;
    for(int i = 1; i < argc; ++i) {
        args.push_back(argv[i]);
    }
    success = true;
    load(args);
}

void Args::load(std::vector<std::string> args) {
    if(first) {
        success = true;
        first = false;
    }
    update = args[0] == "update";
    args.erase(args.begin());

    unsigned int ignore_i = args.size();
    for (unsigned int i = 0; i < args.size(); ++i) {
        if (args[i] == "--output-dir") {
            ++i;
            continue;
        }
        if (args[i] == "--lock-file") {
            ++i;
            continue;
        }
        if (args[i][0] == '-')
            continue;
        ignore_i = i;
        spec_file = args[i];
        spec = load_project_spec(spec_file);
        break;
    }
    for(unsigned int i = 0; i < args.size(); ++i) {
        if (i == ignore_i)
            continue;
        if(starts_with(args[i], "--")) {
            if(args[i] == "--output-dir") {
                spec.output_dir = absdir(args[i+1]);
                ++i;
                continue;
            }
        } else if (args[i] == "--lock-file") {
            spec.lock_file = absdir(args[i+1]);
            ++i;
        } else if(starts_with(args[i], "-D")) {
            // variable
            const char* var = args[i].c_str() + 2;
            const char* val = var;
            while(*val && *val != '=')
                ++val;
            if(*var == '=') {
                ++val;
            }
            if(*val != 0) {
                printf("invalid format missing '=': %s\n", args[i].c_str());
                success = false;
                continue;
            }
            std::string name = std::string(var, val-var-1);
            Version version(val);
            spec.build_config[name] = version;
        } else if(args[i][0] == '-') {
            printf("unknown option %s\n", args[i].c_str());
            success = false;
        } else {
            printf("whats this? %s\n", args[i].c_str());
            success = false;
        }
    }

    if(spec_file.empty())
        success = false;

    if(spec.output_dir.empty())
        success = false;
}


void sync_dir(std::vector<std::string> files, std::string output_dir) {
    std::vector<std::string> list = scan_dir(output_dir);
    std::map<std::string, bool> map;
    for(std::string file : files) {
        map[basename(file)] = true;
    }
    for(const std::string& dir : list) {
        if(!map[dir]) {
            print("removing {}", dir);
            tea::rmdir(output_dir + "/" + dir);
        }
    }
}
void checkout_repo(Package& package, std::string output_dir, Version old_version) {
    // start fresh
    if(is_dir(output_dir))
        tea::rmdir(output_dir);
    mkdir(output_dir);
    TmpPath tmp(output_dir);
    if (old_version)
        csd::print("install {}/{} was {}", package.name, package.version, old_version);
    else
        csd::print("installing {}/{}", package.name, package.version);
    if(is_file(package.artifact.archive)) {
        copy_file(package.artifact.archive, output_dir + "/" + basename(package.artifact.archive));
        tmp.disown();
        return;
    }
    if(package.artifact.remove_nests) {
        package.artifact.archive = find_nest(package.artifact.archive).string();
    }

    DirGlob glob;
    // don't include version control directories
    glob.exclude("/.git/*/.*");
    glob.exclude("/.svn/*/.*");
    glob.exclude("/.hg/*/.*");

    for(std::pair<std::string, std::string> from_to : package.dir_map) {
        std::string dst = output_dir + "/" + from_to.second;
        while(!from_to.first.empty() && from_to.first[from_to.first.size()-1] == '/')
            from_to.first.resize(from_to.first.size()-1);
        glob.set_glob_expression(from_to.first + "/*/.*");
        glob.copy_dir(package.artifact.archive, dst, false, package.dev);
    }

    for(const std::string& exclude : package.exclude_files)
        glob.exclude(exclude);

    for(FileMap& from_to : package.file_map) {
        glob.set_glob_expression(from_to.source);
        std::string dst = output_dir + "/" + from_to.destination;
        glob.copy_dir(package.artifact.archive, dst, from_to.flat, package.dev);
    }
    // We do this last in case a glob expression overwritten this
    std::string teaspec = join_path(output_dir, package.name + ".teaspec");
    if(package.dev) {
        tea::rmdir(teaspec);
        symlink(package.spec_file, teaspec);
    } else {
        files_readonly_recurse(output_dir);
        if (path_exists(package.spec_file))
            copy_file(package.spec_file, teaspec);
        else {
            file_put_contents_nl(teaspec, package.js.dump(4));
        }
    }
    tmp.disown();
}

namespace tea {

    TeaspecInstaller::TeaspecInstaller(ProjectSpec& spec_in, bool update) {
        this->spec = spec_in;
        this->update = update;
        artifact_cache_dir = init_cache_dir();
        database = init_cache_db();
        string_cache = StringCache(database);
        cache = DirCache(database, artifact_cache_dir);

        // we do this first so the specs is modified by the SpecResolveDb
        resolver.add_finder(SpecResolveDb(&string_cache, spec));


        auto artifact_loader = spec.artifact_loader;
        for (auto& str : artifact_loader) {
            str = replace_env_vars(str);
        }
        artifactLoader.set_command(artifact_loader);
        for(Repo repo : spec.repos) {
            if(!repo.dir.empty()) {
                DirResolveDb dirRepo;
                dirRepo.string_cache = &string_cache;
                dirRepo.base_dir = absdir(repo.dir, spec.base_dir);
                dirRepo.config = spec.build_config;
                resolver.add_finder(dirRepo);
            } else if (!repo.script.empty()) {
                ScriptResolve script;
                script.string_cache = &string_cache;
                script.script = repo.script;
                script.config = spec.build_config;
                resolver.add_finder(script);
            } else {
                Artifact artifact;
                artifact.git_url = repo.git;
                artifact.archive = repo.archive;
                std::string output_dir = artifactLoader.resolve(artifact, cache);
                cache.set_kind(artifact_key(artifact), "repo");
                DirResolveDb repo;
                repo.string_cache = &string_cache;
                repo.base_dir = output_dir;
                repo.config = spec.build_config;
                resolver.add_finder(repo);
            }
        }
    }

    void TeaspecInstaller::load_lock_file() {
        if(is_file(spec.lock_file)) {
            locked_versions = tea::load_lock_file(spec.lock_file).dependencies;
            using_lock = true;
            // having it sorted makes comparisons easier
            sort_list(locked_versions, [] (const Dependency& a, const Dependency& b) -> bool {
                return a.name < b.name;
            });
        }
    }
    void TeaspecInstaller::resolve() {
        if (update) {
            resolved = resolver.resolve(spec.dependencies, {});
        } else {
            auto keep_versions = locked_versions;
            for (Dependency& dep : spec.dependencies) {
                for (size_t i = 0; i < keep_versions.size(); ++i) {
                    auto& keep = keep_versions[i];
                    if (dep.name != keep.name)
                        continue;
                    if (!dep.constraint.check(keep.constraint.min_version)) {
                        keep_versions.erase(keep_versions.begin()+i);
                        break;
                    }
                }
            }
            resolved = resolver.resolve(spec.dependencies, keep_versions);
        }
    }

    int TeaspecInstaller::install() {
        // don't continue if there are resolve errors
        if(log_error_count()) {
            return 1;
        }
        sort_list(resolved, [](const Package& a, const Package& b) -> bool {
            return a.name < b.name;
        });

        std::map<std::string, Version> old_versions;
        // there is new stuff so we must update lock
        if(resolved.size() != locked_versions.size()) {
            using_lock = false;
        } else {
            for (std::size_t i = 0; i < resolved.size(); ++i) {
                if (locked_versions[i].name != resolved[i].name) {
                    using_lock = false;
                    break;
                }
                if (!locked_versions[i].constraint.check(resolved[i].version)) {
                    using_lock = false;
                    break;
                }
            }
        }

        for (Package& package : resolved) {
            for (Dependency& dep : locked_versions) {
                if (dep.name != package.name)
                    continue;
                old_versions[dep.name] = dep.constraint.min_version;
            }
        }

        // First resolve all packages
        for(Package& package : resolved) {
            std::string loaded_dir = artifactLoader.resolve(package.artifact, cache);
            if (!loaded_dir.empty())
                package.artifact.archive = loaded_dir;
        }

        if(!is_dir(spec.output_dir)) {
            mkdir(spec.output_dir);
        }
        if(!is_dir(spec.output_dir)) {
            log_message("E1001", "could not create directory " + spec.output_dir);
            return 1;
        }

        // now we resolve into specified directory
        for(Package package : resolved) {
            std::string package_dir;
            if(package.artifact.archive.empty()) {
                // error
                log_message("F1005", "package archive is empty");
                return 1;
            }
            if(!path_exists(package.artifact.archive)) {
                // error
                std::string message = "package " + package.name + " not fully resolved";
                log_message("F1004", message);
                return 1;
            }
            Version old_version;
            if (contains(old_versions, package.name)) {
                old_version = old_versions[package.name];
            }
            std::string output_dir = spec.output_dir;
            output_dir += "/";
            output_dir += package.name;
            // already there so skip it
            if(is_dir(output_dir)) {
                // if the resolution is different we want to update it
                std::string output_spec = join_path(output_dir, package.name + ".teaspec");
                if(old_version == package.version && is_file_same(output_spec, package.spec_file))
                    continue;
                tea::rmdir(output_dir);
            }
            if (old_version != package.version)
                using_lock = false;
            checkout_repo(package, output_dir, old_version);
        }
        std::vector<std::string> to_keep;
        for(Package& package : resolved) {
            to_keep.push_back(package.name);
        }
        json resolved_js = tea::to_json_lock(resolved);
        json meta_data;
        meta_data["order"] = resolved_js["order"];
        to_keep.push_back("meta.json");
        file_put_contents_nl(join_path(spec.output_dir, "meta.json"), meta_data.dump(4));
        sync_dir(to_keep, spec.output_dir);

        if(!using_lock) {
            resolved_js.erase(resolved_js.find("order"));
            file_put_contents_nl(spec.lock_file, resolved_js.dump(4));
        }
        return 0;
    }

}
int main_install(std::vector<std::string> argsIn) {
    Args args;
    args.load(argsIn);
    for(std::string& str : argsIn) {
        if(str == "--help") {
            return print_help();
        }
    }
    if(!args)
        return print_help();

    TeaspecInstaller installer(args.spec, args.update);
    installer.load_lock_file();
    installer.resolve();
    return installer.install();
}
