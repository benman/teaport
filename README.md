@mainpage

# About

teaport is a cocoapods inspired dependency manager. It's aim is to do only
dependencies. No integration with your tools at all. The inspiration from
cocoapods is the structure of dependencies and how simple it is to have
your own private repo. Any git repository can be made into a teaport repository
in a similar fashion as cocoapods.

# General dependency manager

This is very general and yet specific.

- supports various variants for a package
- versions
- version lock file to reproduce same versions. not necessarily same variant.
- only 1 version will be checked out. Being able to checkout multiple versions
  of a single project is a lot of work from a design and implementation perspective
  and there is already ivy which can do this. Ivy is really great, but checking
  out multiple versions & variants is not always desirable. Ivy is great and
  being able to checkout multiple versions of the same project is a problem for
  me which is why I'm building this.
- use existing tools for downloading. The nice thing of using an existing tool
  is no reimplementation of the wheel is possible. Some dependency managers
  reimplement ssh downloading and they don't even check with the ssh-agent. I
  rather the dependency manager just do dependency managing & make use of other
  tools like scp, ssh, rsync to do the downloading. This has the downside of being
  harder to be cross platform perhaps, I will see how it goes. This also has the
  downside that if the tool doesn't exist the functionality won't be present
  which adds to dependency hell. The solution is good documentation which is hard,
  and good tools to try and download the tools for you, or just tell the user
  to get it themselfs and let them figure it out how to do it. I'm going with the
  aproach of tryin to have documentation and let the user figure out how to get
  the tool on their system. On same platforms there are multiple ways to get
  the tool so I can't assume a path.
- added squirrel-lang interpreter to have a defined interpretter already built in
  so that there is a base for cross scripting. You can launch your own intepreter
  from it like python or just use python instead.

# build instructions

```
mkdir build
cd build
cmake ../ -DCMAKE_BUILD_TYPE=Release
make
# as root
make install
```

Tested compilation with the following:

- clang
- AppleClang
- MSVC
- g++ 8.0+

# hello world example

project.teaspec

```
{
    "repos":[
        {"git":"git@bitbucket.org:benman/teaport-cpp-spec.git"}
    ],
    "dependencies": {
        "nlohmann_json": "+"
    }
}
```

helloworld.cpp

```
#include <nlohmann/json.hpp>
#include <iostream>

int main() {
    nlohmann::json hello;
    hello["ok"] = "world";

    std::cout << hello.dump(4) << '\n';
}
```

CMakelists.txt
```
cmake_minimum_required(VERSION 3.6)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Release/Debug")
endif()

project(helloworld VERSION 1.0.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(NOT MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++17")
endif()




add_executable(helloworld helloworld.cpp)

target_include_directories(helloworld PUBLIC
    dependencies/nlohmann_json
)
if(MSVC)
    target_compile_options(helloworld PUBLIC -Zc:__cplusplus)
endif()

```

Now to compile

```
teaport install project.teaspec --output-dir dependencies
mkdir build
cd build
cmake ..
make
./hellworld
```

# install teaport

```
mkdir build
cd build
cmake ../
make

sudo make install
```

# requirements

these commands must be present on your system

- unzip
- git
- scp
- sftp
- tar
- curl cli

I would love to use rsync, but it's hard to get on windows.

# config

The config file is in `$HOME/.config/teaport/teaport.json`. By default this file
is not created and defaults are used. The following are accepted:

- `cache_dir` - path to where to store cache files. default is `$HOME/.cache/teaport`
  it is safe to use the same cache dir for various variants, sources, and etc..
  an sqlite database is used to map cached file to a directory within the cache.
  If you want to clear a specific item simply look it up in the database and
  delete the directory. Deleting the database file itself will invalidate the
  entire cache. Relative paths are used so you can easily move aronud the cache.

# json files

You can add all the extra json you want so that 1 json file can be shared across
tools. However to allow compatiblity, reduce name collisions, and keep the look
consistent. Please try to follow rules bellow.

- snake case, all lower case key names.
- for your top level element use a 3 letter minimum acronym.
- use url schemes and styles current used by this project.
- don't modify files that user edits. Instead create a new file with overrides,
  to be appended on teaport invocation. Or create a defaults teaspec file that is
  prepended to the list on teaport invocation. This is to reduce suprises of
  files a user changes, and to reduce random version control changes. And to keep
  generated data separate from user created data.
- json files intended to be used with teaport to have extension of .teaspec. If
  your file is never going to be read by teaport use your own extension. This just
  helps in knowing relationships of files to which tool.


# Json Types

## Repo

```
{
    "git": "giturl",
    "dir": "/path/to/repo"
}
```

You can only specify a git url or dir per entry in "repos" list. Git url will
be passed to git command directly. Dir can be absolute or relative to the spec
file.

## Dependency

```
{
    "force": bool default false,
    "dev": Path default null,
    "version": VersionConstraint,
    "ignore": bool default false
}
```

Name    | description
--------|---------------------------------------------------------------------
force   | if true ignores constraints and simply uses the version you specify.
dev     | path to spec file of package you are working in tandem with
version | Version constraint e.g. 2.+
ignore  | If true this package is ignored and will not be resolved.

## VersionConstraint

Version is a string usually of the form "a.b.c". If a plus is used as any
of the a, b, c the highest number possible is used for that spot. If there
is any non-numeric characters then an exact match is required. Only a-z, +, -,
and _ are allowed.

## Version

best practice is to use the form "a.b.c" Where a, b, and c are integers. Any
string can be used. Only a-z, +, -, and _ are allowed.

## Artifact

```
{
    "archive": "/path/to/zip.zip",
    "git": "git_url",
    "tag": "git_tag",
    "extract": bool default true,
    "remove_nests": bool default false
}
```

- archive: can be an http/https url, or path to a zip file. If specified,
  git cannot bet used.
- git: a git url, will be passed to git command.
- tag: the tag of the git url to use.
- extract: when using archive you can set this to false so that the contents
  of the file is not extracted and simply treated as a binary blob.
- remove_nests: If true, nesting is removed until a directory contains atleast 1
  entry. It is useful to specify true when you use an archive and it has
  multiple subfolders with only 1 folder inside. This simplifies dir_map, file_map,
  and exclude_files lists.


## TargetConstraint

```
{
    "name": string,
    "=": Version | string | bool | integer,
    ">=": Version | integer,
    "<": Version | integer
}
```

- name: a config_name used in build_config.
- =: if used >=, and < cannot be used. This means the constraint must match the
  value specified exactly.
- ">=": the config value must be >=.
- <: the config value must be < what's specified.

## FileMap


```
{
    "destination": string,
    "flat": bool default false
}
```

- destination: specifies a path relative to installation directory.
- flat: if false subdirectories are made to mimic that of the source tree. If
  false all files are put in destination directory.

## Project Spec

`Project.teaspec`

```
{
    "repos" : [Repo],
    "dependencies": {package_name: Dependency | Version | LibrarySpec},
    "build_config": {config_name: Version | boolean | integer},
}
```

build_config is intended to describe the compilation target. config_name is
lower case only name of some parameter, it is user defined and can only be
a-z, 0-9, and _ chars. First char cannot be a number. It should map to either
a Version, bool or integer. This is used so that a library spec can advertise
what target it supports. All library specs build_config parameter must be
satisfied for that library spec to be used.


Quick guide:

```
{
    "repos":[
        {"git": "giturl"},
        {"dir":"/path/to/dir"}
    ],
    "dependencies": {
        "SomeLibrarySpecific": "2.+",
        "DevLibrary": "3.+@/path/to/library-project/library.teaspec",
        // or no version necessary
        "DevLibrary2": "@/path/to/DevLibrary2/DevLibrary2.teaspec"
    },
    "build_config": {
        // describe your application environment.
        "ios": "12.2",
        "arch": 64,
        "bitcode": false
    },
}
```

# Library Spec

Each file will describe 1 artifact or repo for dependency.

```
{
    "version": Version,
    "name": package_name,
    "artifact": Artifact,
    "constraints": [TargetConstraint],
    "dependencies": {package_name: Dependency | Version},
    "dir_map": {from_dir -> to_dir: string},
    "file_map": {file_regex -> FileMap | string},
    "exclude_files": [exclude_regex]
}
```

**dir_map** is a mapping of from_dir to to_dir. from_dir is relative to artifact
even when absolute paths are used. to_dir is relative to where the artifact is
installed.

**file_map** uses c++ regex to match files and where to map to. file_regex is
relative to artifact, and FileMap is relative to where it's installed.


## File Regexes

file_regex, exclude_regex use c++ regular expressions and by default
non-recursive. The string is split into 2 pieces, the directory part, and the
file part. You cannot use regular expressions in the directory component. The
file component is the portion after the last "/" character. If no "/" is used
then it's assumed to be at the root of archive, equivalent to having "/" or
"./", prepended. If before the file component is "/*/" then the file regex
is used on all subdirectories recursivly.


Quick Guide:

`MyLibrary.teaspec`

```
{
    "version": "1.0.0",
    "name": "MyLibrary",
    "artifact": {
        // can be a tar to be extracted
        "archive": "zipFile.zip",
        // git can have a tag for specific version
        "git": "giturl",
        "tag": "1.0.0",
        // when true archive points to a zip file to be extracted
        "extract": true,
    }
    // system & designated constraints
    "constraints": [
        // all versions of windows
        {"name":"windows"},
        {"name":"arch", "=": 64},
        // some packages can provide support for multiple variants of systems
        // e.g. 64bit & 32bit in one package, so this structure works in a way
        // to allow multiple constraints specified. In such cases only 1 must
        // match.
        // possible comparisons >=, <, =
        {"name":"ios", ">=", "8.0", "<", "11.0"}],
        // because it has the same name one of the ios configs
        // must match
        {"name":"ios", ">=": "3.0", "<": "4.0"}]
    ],
    "dependencies": [
        "SomeLibrarySpecific": "2.+"
    ]
    // for archives & git. If present only the following
    // will be extracted into output. Dir will be copied as is
    // exclude_files is ignored
    "dir_map": {
        "./dir/in/repo/src": "/src"
    },
    "file_map": [
        // non recursive
        // regex is only applied to file names
        // use c++ standard regex
        // directory structure is maintained when mapping files
        "./dir/in/repo/.*(h|hpp|cpp)": "/",
        // recursive no space after
        "./dir/in/repo/*/.*\\.(h|hpp|cpp)": "/src",
        "./dir/in/repo/*/.*\\.(h|hpp|cpp)": {
            "destination": "/src",
            "flat": false, // if false (default) directory structure is kept
                            // set to true and files will be copied to /src
        }
        
    ],
    // stuff to exclude
    // Regex style just like in file_map
    // only applies to file_map.
    "exclude_files": [
        // this will match any file named "shell.c"
        "/*/shell.c",
        // this will only apply to root level
        "shell.c",
        ".*-gen.c", // also only at root level
        "/somedir/.*-gen.c", // not recursive only in somedir
        // this will ignore only files in the directory from root of repo.
        // /somedir/yay/file will still be included
        "/somedir/.*",
        // this will not include all_dir
        "/all_dir/*/.*",
    ]
}
```

# Installing

To install dependencies you do the install command. If project.teaspec.lock exists
then the versions installed last time will be used. do `teaport update ...` instead
to ignore the lock file.

```
teaport install project.teaspec
```

You can merge multiple specs into one. This is useful to specify your build configs
in seperate files so that dependencies from the main spec can be shared across
all configurations.

```
teaport merge project.teaspec win10.teaspec
```

# publishing

First add a repo. You need to do this only once. The name you give must be a valid
directory name.

```
teaport repo add <repo-name> <url-to-repo>
```

Now lint. It is recomended to do your own linting of your project. teaport
will only lint the spec file.

```
teaport lint project.teaspec
```

Now publish the tea. `repo_name` is the name of the repo added with `teaport repo add`
command.

```
teaport publish project.teaspec repo_name
```


# Release Notes

## 0.28.0

- upgrade squirrel lang to it's master branch
- automatically exclude .git, .svn, .hg directories
- MacOS current tools support everything we need in c++17. Compilation now can
  use default tools.
- you can use `$version` in your `Artifact.archive` It will be replaced with the
  package version.
- You can set `version` to `scan-http://` or `scan-https://` with url of where
  to find links. teaport will scan for links based on Artifact.archive, you must
  have `$version` somewhere in the url of Artifact.archive in such a case.
- all tests pass on windows

## 0.27.0

- `artifact_loader` items are now variable replaced with environment like shell.

## 0.26.0

- searching now is case insensitive
- search doesn't show duplicates
- wrie a meta.json file in dependencies output directory, with a linear order
  of dependencies to help scripts out if they wish to know an order such that
  next element in list has only dependencies of previous items.

## 0.25.0

- new `teaport outdated <specfile>` command to show updates.
- `teaport repo update` now will update all repos. Specify a spec file to pull
  only relevant repos to spec file.
- implemented basic search. do `teaport search <query>`

## 0.24.0

- new `Repo.script` option so you can specify a script to fetch library information
  from your custom repository format. Script should return a json array of
  versions for the requested package. It is scripts responsibility to do
  caching.

## 0.23.0

- when installing report version upgraded from.
- added test for embedded dependencies in spec file.

## 0.22.0

- new `artifact_loader` option to specify a script to do the artifact
  downloading. All artifacts to be downloaded and extracted will go through
  this script.
- `artifact_loader` can specify a script and shebang will be processed on
  windows
- special case for windows python3. if not found check versions for first
  python version greator than 3.0.0 in PATH environment variable.
- fixed different version in spec and lockfile resolution.

## 0.21.0

- optimized cache usage. Downloaded files that are extracted are deleted to
  save space.

## 0.19.0

-   new `base_dir` option which sets all paths to be relative to `base_dir`.

## 0.18.0

-   new json option `lock_file` and `--lock-file` command line option to specify
    which lock file to use explicitly.

## 0.17.0

- `teaport status` now accepts additional commands to check versions of.

## 0.16.0

- `output_dir` in ProjectSpec is supported.
- you can now have a dependency in ProjectSpec specify full location and not
  need a separate file.
- added `teaport status` to show versions of some commands.

# Known bugs

## filesystem::remove_all on windows doesn't always work

I don't understand why. On some windows machines it works on others it doesn't.
Tested on Windows 10.