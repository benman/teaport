#include "shell.hpp"
#include <regex>
#include <nlohmann/json.hpp>

#include "test_config.h"
#include "SafePrintf.hpp"
#include "Log.hpp"
#include "fileutils.hpp"
#include "jsonutils.hpp"
#include <cxxtest/TestSuite.h>
#include "shell.hpp"

using namespace tea;


class BasicSuite : public CxxTest::TestSuite {
public:
    void testOlderVersion() {
        tea::chdir(TEST_DIR "/onedown");
        file_put_contents("version_down.teaspec.lock", R"({
    "dependencies": [
        {
            "name": "deplib",
            "score": 1,
            "version": "1.0.1"
        },
        {
            "name": "oklib",
            "score": 1,
            "version": "1.0.2"
        },
        {
            "name": "yeslib",
            "score": 1,
            "version": "1.0.0"
        }
    ]
})");
        TmpPath path("output");

        int result = tea::system({TEAPORT_EXE, "install", "version_down.teaspec", "--output-dir", "output"});
        if(result != 0) {
            TS_FAIL("failed to teaport install version_down.teaspec");
        }

        nlohmann::json js = load_json_file("version_down.teaspec.lock");
        for (auto& dep : js["dependencies"]) {
            if (dep["name"] == "oklib") {
                TS_ASSERT_EQUALS(dep["version"].get<std::string>(), "1.0.1");
            }
        }
    }

    void testOlderVersion_v2() {
        tea::chdir(TEST_DIR "/onedown");
        file_put_contents("version_down.teaspec.lock", R"({
    "dependencies": {
        "deplib": {
            "score": 1,
            "version": "1.0.1"
        },
        "oklib": {
            "score": 1,
            "version": "1.0.1"
        },
        "yeslib": {
            "score": 1,
            "version": "1.0.0"
        }
    },
    "version_code": 2
})");
        TmpPath path("output");

        int result = tea::system({TEAPORT_EXE, "install", "version_down.teaspec", "--output-dir", "output"});
        if(result != 0) {
            TS_FAIL("failed to teaport install version_down.teaspec");
        }

        nlohmann::json js = load_json_file("version_down.teaspec.lock");
        for (auto& dep : js["dependencies"]) {
            if (dep["name"] == "oklib") {
                TS_ASSERT_EQUALS(dep["version"].get<std::string>(), "1.0.1");
            }
        }
    }
    void testEmbedded() {
        tea::chdir(TEST_DIR "/onedown");
        tea::rmdir("embedded.teaspec.lock");
        TmpPath path("output");

        int result = tea::system({TEAPORT_EXE, "install", "embedded.teaspec", "--output-dir", "output"});
        if(result != 0) {
            TS_FAIL("failed to teaport install embedded.teaspec");
        }
    }

    void testWow() {
        tea::chdir(TEST_DIR "/onedown");
        TmpPath path("output");

        int result = tea::system({TEAPORT_EXE, "install", "specs-wow.teaspec", "--output-dir", "output"});
        if(result != 0) {
            TS_FAIL("failed to teaport install specs.teaspec");
        }
        TmpPath lockFile("specs-wow.teaspec.lock");

        nlohmann::json json = load_json_file("output/yeslib/yeslib.teaspec");
        if (json["constraints"][0]["name"] != "wow") {
            TS_FAIL("did not install wow version");
        }
    }
};


