#include "PackageExtras.hpp"
#include "resolve.hpp"
#include "fileutils.hpp"
#include "container_ops.hpp"

namespace tea {
    void from_json(const json& js, Package& package) {
        if(js.find("name") != js.end())
            package.name = js["name"];
        if(js.find("version") != js.end()) {
            if (js["version"].is_string()) {
                package.version = Version(js["version"].get<std::string>());
            }
        }
        if (package.version.is_str && package.version.version_str.find(':') == std::string::npos)
            package.version_range = package.version;
        else
            package.version_range = "+";
        if (js.find("version_range") != js.end()) {
            if (js["version_range"].is_string()) {
                package.version_range = VersionConstraint(js["version_range"].get<std::string>());
            }
        }
        if(js.find("dependencies") != js.end()) {
            const json& dependencies = js["dependencies"];
            for(json::const_iterator it = dependencies.cbegin(); it != dependencies.cend(); ++it) {
                Dependency dep;
                dep.name = it.key();
                if(it.value().is_string()) {
                    std::string version_str = it.value();
                    std::string::size_type at_pos = version_str.find('@');
                    if(at_pos != std::string::npos) {
                        dep.dev_spec_file = version_str.substr(at_pos+1);
                        version_str.resize(at_pos);
                    }
                    dep.constraint = VersionConstraint(version_str);
                    if (version_str == "ignore") {
                        dep.ignore = true;
                    }
                } else if(it.value().is_object()) {
                    json dep_js = it.value();
                    if(dep_js.count("force"))
                        dep.force = dep_js["force"];
                    if(dep_js.count("ignore"))
                        dep.ignore = dep_js["ignore"];
                    if(dep_js.count("dev"))
                        dep.dev_spec_file = dep_js["dev"];
                    if(dep_js.count("version"))
                        dep.constraint = VersionConstraint(dep_js["version"].get<std::string>());
                }
                package.dependencies.push_back(dep);
            }
        }
        // This will make some orders more consistent in runs.
        sort_list(package.dependencies, [](const Dependency& a, const Dependency& b) -> bool {
            return a.name < b.name;
        });
        from_json(js, static_cast<PackageExtras&>(package));
    }

    void from_json(const json& js, Artifact& artifact) {
        if(js.find("archive") != js.end()) {
            artifact.archive = js["archive"];
            Url url(artifact.archive);
            if(url.scheme.empty()) {
                artifact.archive = expand_path(artifact.archive);
            }
        }
        if(js.find("git") != js.end())
            artifact.git_url        = expand_path((std::string)js["git"]);
        if(js.find("tag") != js.end())
            artifact.tag            = js["tag"];
        if(js.find("extract") != js.end())
            artifact.extract        = js["extract"];
        if(js.find("remove_nests") != js.end())
            artifact.remove_nests   = js["remove_nests"];
    }

    template<typename T>
    void from_json(const json &js, std::map<std::string, T>& map) {
        for(auto it = js.begin(); it != js.end(); ++it) {
            map[it.key()] = it.value();
        }
    }
    void from_json(const json& js, FileMap& map) {
        if(js.is_string()) {
            map.destination = js;
            return;
        }
        if(js.find("source") != js.end())
            map.source = js["source"];
        if(js.find("destination") != js.end())
            map.destination = js["destination"];
        if(js.find("flat") != js.end())
            map.flat = js["flat"];
    }
    void to_json(json& js, const FileMap& map) {
        js["source"]        = map.source;
        js["destination"]   = map.destination;
        js["flat"]          = map.flat;
    }

    void from_json(const json& js, PackageExtras& extras) {
        if(js.find("artifact") != js.end())
            extras.artifact = js["artifact"];
        if(js.find("constraints") != js.end()) {
            for(const json &constraint_js : js["constraints"]) {
                BuildConstraint constraint = constraint_js;
                extras.constraints.push_back(constraint);
            }
        }
        if(js.find("dir_map") != js.end())
            from_json(js["dir_map"], extras.dir_map);
        if(js.find("file_map") != js.end()) {
            json::const_iterator it_end = js["file_map"].cend();
            for(json::const_iterator it = js["file_map"].cbegin(); it != it_end; ++it) {
                FileMap file_map;
                from_json(it.value(), file_map);
                file_map.source = it.key();
                extras.file_map.push_back(file_map);
            }
        }
        if(js.find("exclude_files") != js.end()) {
            if(js["exclude_files"].is_string())
                extras.exclude_files.push_back((std::string)js["exclude_files"]);
            else {
                for(const json& str : js["exclude_files"]) {
                    extras.exclude_files.push_back((std::string)str);
                }
            }
        }
    }

}