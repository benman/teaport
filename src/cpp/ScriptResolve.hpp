#pragma once

#include <nlohmann/json.hpp>
#include "resolve.hpp"
#include "DirResolveDb.hpp"
#include "StringCache.hpp"

namespace tea {
    class ScriptResolve {
    public:
        std::vector<Package> operator()(const Dependency &name);

        StringCache* string_cache;
        BuildConfig config;
        std::string script;
    };
}