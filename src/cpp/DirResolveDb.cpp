#include "DirResolveDb.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <ctime>

#include "fileutils.hpp"
#include "Log.hpp"
#include "SafePrintf.hpp"
#include "stringutils.hpp"
#include "link_scanner.hpp"

namespace tea {

    void from_json(const json& js, BuildConstraint& constraint) {
        constraint.name = js["name"];
        constraint.constraint = VersionConstraint("*");
        if(js.find("<=") != js.end()) {
            log_message("E1006", "<= in constraint is not supported. Specified for " + constraint.name);
        }
        if(js.find("=") != js.end()) {
            constraint.constraint = VersionConstraint(js["="]);
            return;
        }
        if(js.find(">=") != js.end()) {
            constraint.constraint = VersionConstraint(js[">="]);
            constraint.constraint.max_version = Version(0x7FFFFF, 0x7FFFFF, 0x7FFFFF);
        }
        if(js.find("<") != js.end()) {
            constraint.constraint.max_version = Version(js["<"]);
        }
    }

    void from_json(const json& js, Repo& repo) {
        if(js.find("git") != js.end())
            repo.git = js["git"];
        if(js.find("dir") != js.end())
            repo.dir = js["dir"];
        if(js.find("archive") != js.end())
            repo.archive = js["archive"];
        if(js.contains("script"))
            repo.script = js["script"];
    }


    bool ends_width(const std::string &str, const std::string &test_str) {
        return str.substr(str.size() - test_str.size()) == test_str;
    }

    Package load_spec_json(json package_js, const std::string& filepath) {
        Package package = package_js;
        package.js = package_js;
        package.spec_file = filepath;
        Url url(package.artifact.archive);
        std::string dir = dirname(filepath);
        if(!package.artifact.archive.empty() && url.scheme.empty()) {
            package.artifact.archive = absdir(package.artifact.archive, dir);
        }
        for(Dependency& dep : package.dependencies) {
            if(dep.dev_spec_file.empty())
                continue;
            dep.dev_spec_file = absdir(dep.dev_spec_file, dir);
        }
        return package;

    }
    Package load_spec_file(const std::string& full_path) {
        json package_js;
        package_js = load_json_file(full_path);
        if (package_js.empty())
            return Package();

        return load_spec_json(package_js, full_path);
    }

    int BuildConfig::score(const std::vector<BuildConstraint>& constraints) const {
        int score = 1;
        for(const BuildConstraint& constraint : constraints) {
            auto it = find(constraint.name);
            if(it == end())
                return 0;
            if (!constraint.constraint.check(it->second)) {
                return 0;
            }
            ++score;
        }
        return score;
    }

    DirResolveDb::DirResolveDb(StringCache* scache, std::string base_dir, BuildConfig config) {
        this->string_cache  = scache;
        this->base_dir      = base_dir;
        this->config        = config;
    }

    std::vector<Package> DirResolveDb::scan_dir(const std::string &dir, int max_levels) {
        if(max_levels <= 0)
            return {};
        if(!is_dir(dir)) {
            return {};
        }
        std::vector<Package> result;
        for(const std::filesystem::directory_entry& filename : DirIt(dir)) {
            // it can be a directory or a file. If file json is assumed
            std::string full_path = dir + "/" + filename.path().filename().string();
            if(max_levels > 1 && is_dir(full_path)) {
                std::vector<Package> result_sub = scan_dir(full_path, max_levels-1);
                if(result_sub.size() > 0) {
                    result.insert(result.end(), result_sub.begin(), result_sub.end());
                }
            }
            if(!is_file(full_path))
                continue;
            if(!ends_width(full_path, ".teaspec"))
                continue;
            int64_t size = file_size(full_path);
            // if it's too big forget about it
            if(size <= 5 || size > 4000000)
                continue;
            Package package = load_spec_file(full_path);
            if(package.name.empty())
                continue;
            result.push_back(package);
        }
        return result;
    }
    std::vector<Package> DirResolveDb::operator()(const Dependency &name) {
        assert(string_cache != nullptr);
        if(!name.dev_spec_file.empty()) {
            // it's a file
            Package package = load_spec_file(name.dev_spec_file);
            if(package.name.empty())
                return {};
            package.dev = true;
            package.score = 1;
            package.artifact.archive = dirname(name.dev_spec_file);
            return score_packages({package}, config, *string_cache);
        }
        std::string package_dir = this->base_dir + "/" + name.name;
        std::vector<Package> result = scan_dir(package_dir, 2);
        return score_packages(result, config, *string_cache);
    }

    std::string escape_regex(const std::string& regex) {
        std::string result;
        std::string to_escape = ".{}()\\*?[]+";
        for (char ch : regex) {
            if (to_escape.find(ch) != std::string::npos) {
                result += '\\';
            }
            result += ch;
        }
        return result;
    }
    std::vector<Package> score_packages(std::vector<Package> packages, const BuildConfig& config, StringCache& cache) {
        std::vector<Package> result;
        for(Package& package : packages) {
            if (!package.dev) {
                package.score = config.score(package.constraints);
            }
            std::string vstr = to_string(package.version);
            if (vstr.find(':') == std::string::npos) {
                result.push_back(package);
            } else if (starts_with(vstr, "scan-http://") || starts_with(vstr, "scan-https://")) {
                std::string url = vstr.substr(5);

                StringCacheRow crow = cache.get_row(vstr);
                bool use_cache = false;
                bool has_cache = false;
                if (!crow.value.empty() && crow.last_modified > 0) {
                    auto age = time(nullptr) - crow.last_modified;
                    double days = (double)age/60.0/60.0/24.0;
                    use_cache = days < 7.0;
                    has_cache = true;
                }
                std::vector<Version> versions;
                if (!use_cache) {
                    std::string regex = escape_regex(package.artifact.archive);
                    std::map<std::string, std::string> vars;
                    vars["version"] = "(.*)";
                    regex = replace_string_variables(regex, vars);
                    std::vector<std::string> links;
                    try {
                        links = scan_links(url);
                        versions = scan_versions(links, regex);
                    } catch (CommandError& err) {
                        /*  versions will be empty so nothing to do here
                            but to ignore it. We failed downloading so will
                            try cache.
                        */
                    }

                    std::string cache_value = "";
                    for (auto version : versions) {
                        cache_value += to_string(version);
                        cache_value += "\n";
                    }
                    if (!cache_value.empty())
                        cache.set(vstr, cache_value);
                }

                if (has_cache && versions.empty()) {
                    for (auto& str : tea::split(crow.value, '\n')) {
                        if (!str.empty())
                            versions.push_back(Version(str));
                    }
                }
                for (auto& version : versions) {
                    Package new_package = package;
                    new_package.version = version;
                    result.push_back(new_package);
                }
            }
        }

        for(auto& package : result) {
            std::map<std::string, std::string> vars;
            vars["version"] = to_string(package.version);
            package.artifact.archive = replace_string_variables(package.artifact.archive, vars);
            package.artifact.tag = replace_string_variables(package.artifact.tag, vars);
            package.artifact.git_url = replace_string_variables(package.artifact.git_url, vars);

        }
        return result;
    }
}