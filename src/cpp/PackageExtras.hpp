#pragma once

#include <nlohmann/json.hpp>

#include "Version.hpp"

namespace tea {
    using nlohmann::json;

    class BuildConstraint {
    public:
        std::string name;

        VersionConstraint constraint;
    };

    void from_json(const json& js, BuildConstraint& constraint);
    void to_json(json& js, const BuildConstraint& constraint);

    struct Artifact {
        /** Path to zip file, directory or url to zip file */
        std::string archive;
        std::string git_url;
        std::string tag;

        /** If true archive points to zip file */
        bool        extract         = true;
        bool        remove_nests    = false;

        explicit operator bool() const {
            return !archive.empty() || !git_url.empty();
        }

    };
    void from_json(const json& js, Artifact& constraint);
    void to_json(json& js, const Artifact& constraint);

    struct FileMap {
        std::string     source;
        std::string     destination;
        bool            flat = false;
    };

    void from_json(const json& js, FileMap& map);
    void to_json(json& js, const FileMap& map);

    struct PackageExtras {
        VersionConstraint version_range;
        Artifact artifact;
        std::vector<BuildConstraint> constraints;
        std::map<std::string, std::string> dir_map;
        std::vector<FileMap> file_map;

        std::vector<std::string> exclude_files;
        std::string spec_file;
        nlohmann::json js;
        /** if set to true link files to original files so that changes
            are saved to original rather than copy.
        */
        bool dev = false;
    };

    class Package;
    void from_json(const json& js, Package& package);
    void from_json(const json& js, PackageExtras& constraint);
    void to_json(json& js, const PackageExtras& constraint);

}