#include <string>
#include <nlohmann/json.hpp>
#include <httplib.h>

#include "exceptions.hpp"
#include "fileutils.hpp"
#include "lint.hpp"
#include "SafePrintf.hpp"

using nlohmann::json;
using namespace tea;
using namespace csd;

static int print_help(){
const char* str =
R"(
    server
        Starts web server
)";

    printf("%s", str);
    return 1;
}


int main_server(std::vector<std::string> args) {
    args.erase(args.begin());
    if(args.size() < 1)
        return print_help();
    for(std::string& arg : args) {
        if(arg == "--help") {
            return print_help();
        }
    }
    int port = 8080;
    using namespace httplib;
    Server server;
    std::thread thread([&server]() {
        while (true) {
            int signal = wait_for_signal();
            csd::print("got signal {}", signal);
            break;
        }
        server.stop();
    });
    thread.detach();
    server.set_base_dir("./");

    csd::print("starting server http://localhost:{}}/", port);
    server.listen("localhost", port);
    return 0;
}