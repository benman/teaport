#include "shell.hpp"
#include <regex>
#include <nlohmann/json.hpp>

#include "test_config.h"
#include "SafePrintf.hpp"
#include "Log.hpp"
#include "fileutils.hpp"
#include "jsonutils.hpp"
#include <cxxtest/TestSuite.h>
#include "shell.hpp"
#include "container_ops.hpp"
#include "git.hpp"

using namespace tea;

bool is_equal(const CommandLine& a, const CommandLine& b) {
    if (a.size() != b.size())
        return false;
    for (std::size_t i = 0; i < a.size(); ++i) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}

class BasicSuite : public CxxTest::TestSuite {
public:

    void testNotFoundInstall() {
        tea::chdir(TEST_DIR "/basic");
        TmpPath path("output");
        TmpPath lockFile("notfound.teaspec.lock");

        int result = tea::system({TEAPORT_EXE, "install", "notfound.teaspec", "--output-dir", "output"});
        if(result == 0) {
            TS_FAIL("teaport install notfound.teaspec should fail");
        }
    }
    void testInstall() {
        tea::chdir(TEST_DIR "/basic");
        TmpPath path("output");

        int result = tea::system({TEAPORT_EXE, "install", "specs.teaspec", "--output-dir", "output"});
        if(result != 0) {
            TS_FAIL("failed to teaport install specs.teaspec");
        }
        TmpPath lockFile("specs.teaspec.lock");

        nlohmann::json json = load_json_file("output/ubuild/hello");
        if(json["hello"] != "world") {
            TS_FAIL("did not extract dev spec ubuild/hello properly");
        }
    }

    void testRemovedPackage() {
        tea::chdir(TEST_DIR "/basic");
        TmpPath path("output");
        copy_file("removed.teaspec.lock.test", "removed.teaspec.lock");
        int result = tea::system({TEAPORT_EXE, "install", "removed.teaspec", "--output-dir", "output"});
        TS_ASSERT_EQUALS(result, 0);
    }

    void testGitHash() {
        std::string hash = tea::git_get_commit_hash(PROJECT_SOURCE_DIR);
        TS_TRACE("got git hash of " + hash);
        TS_ASSERT_EQUALS(hash.size(), 40);
    }

    void testJsonComments() {
        std::string js_str = R"(
            {
                /* a comment */
                // "hello": "world2"
                "hello": "world"
            }
        )";

        try {
            clean_json_comments(js_str);
            nlohmann::json js = nlohmann::json::parse(js_str);
            TS_ASSERT_EQUALS(js["hello"], "world");
        } catch (std::exception& e) {
            TS_FAIL("could not parse with comments");
        }
    }

    void testRmDir() {
        mkdir("rmtest");
        mkdir("rmtest/hello");
        file_put_contents("rmtest/ya", "ok111");
        file_put_contents("rmtest/hello/bb", "yay");
        file_put_contents("rmtest/hello/cc", "yayok");

        std::string ok = file_get_contents("rmtest/hello/cc");
        TS_ASSERT_EQUALS(ok, "yayok");

        TS_ASSERT(is_dir("rmtest"));
        TS_ASSERT(tea::rmdir("rmtest"));
        TS_ASSERT(!is_dir("rmtest"));
    }
    void testSortList() {
        std::vector<std::string> list {"4", "3", "1", "2", "5", "2"};
        sort_list(list, [](const std::string& a, const std::string& b) -> bool {
            return a < b;
        });

        TS_ASSERT_EQUALS(list, (std::vector<std::string>{"1", "2", "2", "3", "4", "5"}));

    }
    void testParseSheBang() {
        CommandLine line = parse_shebang("#!/usr/bin/env python3");
        TS_ASSERT_EQUALS(line, (CommandLine{"/usr/bin/env", "python3"}));

        // spaces are allowed in the start
        line = parse_shebang("#!   /usr/bin/env python3");
        TS_ASSERT_EQUALS(line, (CommandLine{"/usr/bin/env", "python3"}));
    }

    void testRepoUpdate() {
        // a basic invocation shouldn't fail
        int exit_code = system({TEAPORT_EXE, "repo", "update"});
        TS_ASSERT_EQUALS(exit_code, 0);
    }
};


