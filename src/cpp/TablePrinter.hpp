#pragma once

#include <vector>
#include <string>

namespace tea {
    inline std::string pad_right(std::string input, std::size_t size) {
        while (input.size() < size) {
            input += ' ';
        }
        return input;
    }
    struct TablePrinter {
        typedef std::vector<std::string> Row;

        void push_back(const Row& row) {mRows.push_back(row);}
        void set_header(Row header) {mHeader = header;}

        void print_cli() {
            std::vector<int> sizes;
            for (std::size_t i = 0; i < mHeader.size(); ++i) {
                sizes.push_back(mHeader[i].size()+1);
            }
            for (Row& row : mRows) {
                for (std::size_t i = 0; i < row.size(); ++i) {
                    sizes[i] = std::max<int>(sizes[i], row[i].size()+1);
                }
            }

            for (int& size : sizes) {
                int new_size = size/4;
                new_size *= 4;
                if (new_size < size)
                    new_size += 4;
                size = new_size;
            }

            for (std::size_t i = 0; i < mHeader.size(); ++i) {
                std::cout << pad_right(mHeader[i], sizes[i]);
            }
            std::cout << "\n";
            int columns = mHeader.size();
            for (Row& row : mRows) {
                for (int i = 0; i < columns-1; ++i) {
                    std::cout << pad_right(row[i], sizes[i]);
                }
                std::cout << row[row.size()-1];
                std::cout << '\n';
            }
        }
        // TODO: print_json
        // TODO: variables should be private
        Row mHeader;
        std::vector<Row> mRows;
    };
}