#include "ScriptResolve.hpp"

#include "shell.hpp"
#include "fileutils.hpp"

namespace tea {
    std::vector<Package> ScriptResolve::operator()(const Dependency &dependency) {
        assert(string_cache != nullptr);
        if (!dependency.dev_spec_file.empty())
            return {};
        CommandLine command{this->script, "search", dependency.name};


        CompletedProcess completed = system_capture(command);
        if (!completed || completed.stdout_data.empty())
            return {};

        completed.stdout_data.push_back(0);
        nlohmann::json packageJsList = parse_json(reinterpret_cast<char*>(&completed.stdout_data[0]));
        std::vector<Package> result;
        for(nlohmann::json& package : packageJsList) {
            result.push_back(load_spec_json(package, "/"));
        }
        return score_packages(result, config, *string_cache);
    }
}