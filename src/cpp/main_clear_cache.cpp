#include <string>
#include <nlohmann/json.hpp>

#include "fileutils.hpp"
#include "tea_dir.hpp"

using nlohmann::json;

using namespace tea;
static int print_help() {
const char* str = R"(teaport clear cache
    deletes the cache directory. You can do it yourself too manually. Current
    cache dir is at {}
)";
    csd::print(str, cache_dir());
    return 1;
}

int main_clear_cache(std::vector<std::string> args) {
    if(args.size() != 2 || args[0] != "clear" || args[1] != "cache") {
        return print_help();
    }
    csd::print("deleting cache dir {} ...", cache_dir());
    bool success = rmdir(cache_dir());
    csd::print(success? "Done" : "Failed to delete cache");
    return success? 0 : 1;
}