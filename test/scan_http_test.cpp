#include "shell.hpp"
#include <regex>
#include <nlohmann/json.hpp>

#include "test_config.h"
#include "stringutils.hpp"
#include "fileutils.hpp"

using namespace tea;

bool is_equal(const CommandLine& a, const CommandLine& b) {
    if (a.size() != b.size())
        return false;
    for (std::size_t i = 0; i < a.size(); ++i) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}

Version detect_version(const std::string& file_data) {
    auto parts = tea::split(file_data, '\n');
    for (auto& line : parts) {
        if (line.find("version") != std::string::npos) {
            auto more_parts = tea::split(line, ' ');
            for (auto& vtest : more_parts) {
                Version version = vtest;
                if (version.is_str)
                    continue;
                return version;
            }
        }
    }
    return {};
}

Version detect_version_file(const std::string& filepath) {
    return detect_version(file_get_contents(filepath));
}
class ScanHttpSuite : public CxxTest::TestSuite {
public:
    void testUpgrade() {
        tea::chdir(TEST_DIR "/scan-https");
        auto specs_lock = R"(
{
    "dependencies": {
        "nlohmann_json": {
            "score": 1,
            "version": "3.6.1"
        }
    },
    "version_code": 2
}
        )";
        file_put_contents("specs.teaspec.lock", specs_lock);
        if (tea::path_exists("output"))
            tea::rmdir("output");
        tea::TmpPath tmp_output("output");
        // we first install to populate output/ dir
        system_checked({TEAPORT_EXE, "install", "specs.teaspec"});
        std::string contents = file_get_contents("output/nlohmann_json/nlohmann/json.hpp");
        Version version = detect_version(contents);
        TS_ASSERT_EQUALS(version, "3.6.1");

        system_checked({TEAPORT_EXE, "update", "specs.teaspec"});
        auto js = load_json_file("specs.teaspec.lock");
        version = js["dependencies"]["nlohmann_json"]["version"].get<std::string>();
        TS_ASSERT(version > "3.7.0");

        Version real_version = detect_version_file("output/nlohmann_json/nlohmann/json.hpp");
        TS_ASSERT_EQUALS(version, real_version);
    }

    void testDowngrade() {
        auto specs_lock = R"(
{
    "dependencies": {
        "nlohmann_json": {
            "score": 1,
            "version": "3.7.3"
        }
    },
    "version_code": 2
}
        )";
        file_put_contents("specs.teaspec.lock", specs_lock);
        file_put_contents("specs-downgrade.teaspec.lock", specs_lock);
        tea::rmdir("output");
        tea::TmpPath tmp_output("output");

        // we first install to populate output/ dir
        system_checked({TEAPORT_EXE, "install", "specs.teaspec"});
        std::string contents = file_get_contents("output/nlohmann_json/nlohmann/json.hpp");
        Version version = detect_version(contents);
        TS_ASSERT_EQUALS(version, "3.7.3");

        system_checked({TEAPORT_EXE, "install", "specs-downgrade.teaspec"});
        auto js = load_json_file("specs-downgrade.teaspec.lock");
        version = js["dependencies"]["nlohmann_json"]["version"].get<std::string>();
        TS_ASSERT_EQUALS(version, "3.6.1");

        Version real_version = detect_version_file("output/nlohmann_json/nlohmann/json.hpp");
        TS_ASSERT_EQUALS(version, real_version);
    }
};


