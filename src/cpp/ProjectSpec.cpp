#include "ProjectSpec.hpp"

#include "shell.hpp"
#include "fileutils.hpp"
#include "tea_dir.hpp"
#include "hash.hpp"
#include "Log.hpp"
#include "stringutils.hpp"

namespace tea {

    std::string get_extension(const std::string& filename) {
        std::string::size_type slash_pos = filename.rfind('/');
        std::string::size_type first_dot = filename.find('.', slash_pos == std::string::npos? 0 : slash_pos);
        if (first_dot == std::string::npos)
            return "";
        return filename.substr(first_dot);
    }
    std::string artifact_key(Artifact artifact) {
        if (!artifact)
            return "";
        std::string key;
        if (!artifact.archive.empty()) {
            key += "archive=";
            key += artifact.archive;
            key += ";";
        }
        if (!artifact.git_url.empty()) {
            key += "git_url=";
            key += artifact.git_url;
            key += ";";
        }
        if (!artifact.tag.empty()) {
            key += "tag=";
            key += artifact.tag;
            key += ";";
        }

        if (artifact.extract)
            key += "extract=true;";
        if (artifact.remove_nests)
            key += "remove_nests=true";
        return key;
    }
    std::string resolve_archive(std::string archive, std::string output_dir) {
        if(is_dir(output_dir))
            return output_dir;
        if (archive == output_dir) {
            throw IOError("it doesn't make sense for archive == output_dir");
        }

        TmpPath path(output_dir);
        tea::mkdir(output_dir);
        if(!unzip(archive, output_dir)) {
            throw IOError("could not unzip");
        }
        path.disown();
        return output_dir;
    }

    void download_http_file(std::string url_str, std::string output_file) {
        TmpPath tmp(output_file);

        if(!is_file(output_file)) {
            csd::print("> curl {}", url_str);
            tea::system_checked({"curl", "-f", "-s", "--show-error", "-L", url_str, "-o", output_file});
        }
        tmp.disown();

    }
    std::string resolve_url(std::string url_str, bool extract, std::string output_dir) {
        if(is_dir(url_str))
            return url_str;
        if(is_dir(output_dir)) {
            return output_dir;
        }

        Url url(url_str);
        std::string filename = basename(url_str);

        std::string output_file = init_http_cache_dir() + "/" + filename.substr(0, 8);
        std::string hash = pod2hex_string(simple_hash32(url_str));
        output_file += "-" + hash + get_extension(filename);
        TmpPath tmp(output_file);
        if(url.scheme == "http" || url.scheme == "https") {
            download_http_file(url_str, output_file);
            url_str = output_file;
        }
        if(url.scheme == "ssh") {
            std::vector<std::string> args{"scp"};
            if(url.port > 0) {
                args.push_back("-P");
                args.push_back(std::to_string(url.port));
            }
            std::string url_arg;
            if(!url.user.empty()) {
                url_arg = url.user;
                url_arg += "@";
            }
            url_arg += url.domain;
            url_arg += ":";
            url_arg += url.path;
            args.push_back(url_arg);
            args.push_back(output_file);
            csd::print("> scp {}", url_arg);
            tea::system_checked(args);

            url_str = output_file;
        }
        if(is_file(url_str)) {
            if(extract)
                return resolve_archive(url_str, output_dir);
            else {
                mkdir(output_dir);
                std::filesystem::rename(url_str, output_dir + "/" + filename);
            }
            url_str = output_dir;
        }
        if(is_dir(url_str))
            return url_str;
        return "";
    }

    std::string resolve_git(std::string git_url, std::string tag, std::string output_dir) {
        std::string key = git_url + "#" + tag;
        if(tag.empty())
            key = git_url;

        if(is_dir(output_dir))
            return output_dir;

        std::vector<std::string> command;
        command.insert(command.end(), {"git", "clone", "--depth", "1", "--single-branch"});

        if(!tag.empty()){
            command.insert(command.end(), {"--branch", tag});
        }
        command.insert(command.end(), {git_url, output_dir});
        csd::print("git clone {}", git_url);
        int result = system_checked(command);
        if(result == 0)
            return output_dir;
        std::string message = "could not clone ";
        message += git_url;
        throw IOError(message);
    }



    Artifact BasicArtifactLoader::download(Artifact artifact, std::string output_dir) {
        // this must lead to a zip, tar etc..
        if(!artifact.archive.empty()) {
            artifact.archive = resolve_url(artifact.archive, artifact.extract, output_dir);
        } else if(!artifact.git_url.empty()) {
            std::string resolved_dir = resolve_git(artifact.git_url, artifact.tag, output_dir);
            artifact.archive = resolved_dir;
        }

        return artifact;
    }



    CommandArtifactLoader::CommandArtifactLoader(){}
    CommandArtifactLoader::CommandArtifactLoader(std::vector<std::string> command)
        : mCommand(command) {

    }

    Artifact CommandArtifactLoader::download(Artifact artifact, std::string output_dir) {
        if (mCommand.empty())
            return {};
        std::vector<std::string> command = mCommand;

        if (!artifact.archive.empty()) {
            command.push_back("archive=" + artifact.archive);
        }
        if (!artifact.git_url.empty()) {
            command.push_back("git_url=" + artifact.git_url);
        }
        if (!artifact.tag.empty()) {
            command.push_back("tag=" + artifact.tag);
        }
        command.push_back("output=" + output_dir);

        int failure = system(command);
        if (failure || !path_exists(output_dir))
            return {};
        artifact.archive = output_dir;
        return artifact;
    }


    void CommandArtifactLoader::set_command(std::vector<std::string> command) {
        mCommand = command;
    }

    std::vector<std::string> CommandArtifactLoader::get_command() const {
        return mCommand;
    }

    Artifact CompoundArtifactLoader::download(Artifact artifact, std::string output_dir) {
        std::string loading = output_dir + ".loading";
        TmpPath tmpLoading(loading);
        Artifact next = mCommandLoader.download(artifact, loading);
        if (next) {
            if (is_dir(loading)) {
                std::filesystem::rename(loading, output_dir);
                next.archive = output_dir;
                return next;
            }
        } else next = artifact;
        next = mDefaultLoader.download(next, output_dir);
        return next;
    }

    void CompoundArtifactLoader::set_command(std::vector<std::string> command) {
        mCommandLoader.set_command(command);
    }

    CommandLine CompoundArtifactLoader::get_command() const {
        return mCommandLoader.get_command();
    }


    std::string ArtifactLoader::resolve(Artifact artifact, DirCache& cache) {
        std::string key = artifact_key(artifact);
        if (key.empty()) {
            csd::print("empty artifact :(");
            return "";
        }

        std::string output_dir = cache.dir_for_key(key);
        if (path_exists(output_dir))
            return output_dir;
        if (is_dir(artifact.archive))
            return artifact.archive;
        TmpPath tmpPath(output_dir);
        Artifact resolved = download(artifact, output_dir);
        if (!resolved.archive.empty())
            tmpPath.disown();
        return resolved.archive;
    }

    ProjectSpec load_project_spec(std::string file) {
        nlohmann::json js = load_json_file(file);
        if(js.empty()) {
            log_message("F1006", "loaded spec is empty");
            return {};
        }
        ProjectSpec projectSpec;
        static_cast<Package&>(projectSpec) = load_spec_json(js, file);

        projectSpec.base_dir = dirname(file);
        if (js.contains("base_dir"))
            projectSpec.base_dir = absdir(js["base_dir"], projectSpec.base_dir);
        if (js.contains("repos")) {
            for (auto& repo_js : js["repos"]) {
                Repo repo = repo_js;
                if (!repo.dir.empty())
                    repo.dir = absdir(repo.dir, projectSpec.base_dir);
                if (!repo.script.empty())
                    repo.script = absdir(repo.script, projectSpec.base_dir);
                projectSpec.repos.push_back(repo);
            }
        }

        projectSpec.lock_file = file + ".lock";

        if (js.contains("lock_file"))
            projectSpec.lock_file =  absdir(js["lock_file"], projectSpec.base_dir);
        if (js.contains("output_dir"))
            projectSpec.output_dir = absdir(js["output_dir"], projectSpec.base_dir);

        if (js.contains("build_config")) {
            for(json::iterator it = js["build_config"].begin(); it != js["build_config"].end(); ++it) {
                if(it.value().is_string())
                    projectSpec.build_config[it.key()] = Version(it.value().get<std::string>());
                else if(it.value().is_boolean())
                    projectSpec.build_config[it.key()] = Version(it.value()? 1 : 0, 0, 0);
                else if(it.value().is_number())
                    projectSpec.build_config[it.key()] = Version(it.value().get<int>(), 0, 0);
            }

        }
        if (js.contains("artifact_loader")) {
            for(auto& item : js["artifact_loader"]) {
                projectSpec.artifact_loader.push_back(item.get<std::string>());
            }
            if (starts_with(projectSpec.artifact_loader[0], "./") ||
                projectSpec.artifact_loader[0].find('/') != std::string::npos) {
                projectSpec.artifact_loader[0] = absdir(projectSpec.artifact_loader[0], projectSpec.base_dir);
            }
        }
        return projectSpec;

    }
}