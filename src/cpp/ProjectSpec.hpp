#pragma once

#include "resolve.hpp"
#include "DirCache.hpp"
#include "DirResolveDb.hpp"
#include "nlohmann/json.hpp"

namespace tea {

    /** @return a key representation of the artifact suitable for a map */
    std::string artifact_key(Artifact artifact);
    class ArtifactLoader {
    public:
        /** @return true if successfully handled */
        virtual Artifact download(Artifact artifact, std::string output_Dir)=0;
        virtual std::string resolve(Artifact artifact, DirCache& cache);
    };

    class BasicArtifactLoader : public ArtifactLoader {
    public:
        virtual Artifact download(Artifact artifact, std::string output_dir);
    };

    class CommandArtifactLoader : public ArtifactLoader {
    public:
        CommandArtifactLoader();
        CommandArtifactLoader(std::vector<std::string> command);

        virtual Artifact download(Artifact artifact, std::string output_dir);

        void set_command(std::vector<std::string> command);
        std::vector<std::string> get_command() const;

    private:
        std::vector<std::string> mCommand;
    };

    class CompoundArtifactLoader : public ArtifactLoader {
    public:
        virtual Artifact download(Artifact artifact, std::string output_dir);

        void set_command(std::vector<std::string> command);
        std::vector<std::string> get_command() const;

    private:
        BasicArtifactLoader     mDefaultLoader;
        CommandArtifactLoader   mCommandLoader;
    };

    class ProjectSpec : public Package {
    public:
        std::vector<Repo>               repos;
        BuildConfig                     build_config;
        // if you want more in a chain, write a script and make that be your
        // loader.
        std::vector<std::string>        artifact_loader;

        std::string                     lock_file;
        std::string                     output_dir;
        std::string                     base_dir;
    };

    ProjectSpec load_project_spec(std::string file);
}