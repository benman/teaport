#include "Sqlite3.hpp"
#include "fileutils.hpp"
#include "tea_dir.hpp"
#include "shell.hpp"
#include <nlohmann/json.hpp>
#include "resolve.hpp"
#include "DirCache.hpp"
#include "Log.hpp"
#include "stringutils.hpp"
#include "DirResolveDb.hpp"
#include "TablePrinter.hpp"
#include "container_ops.hpp"

using namespace tea;
static int print_help() {
const char* str=
R"(teaport search <query> - search repos for a package
    query   search string

)";
    printf("%s", str);
    return 1;
}

std::string repo_dir_from(const Package& package) {
    std::string dir = package.spec_file;
    dir = dirname(dirname(dirname(dir)));
    return dir;
}
struct SearchResult {
    DirCacheDbEntry cache_entry;
    std::string     package_name;
    Version         latest_version;
};

static int search(std::string query) {
    Sqlite3 cache_db = init_cache_db();
    DirCache cache(cache_db, init_cache_dir());
    StringCache string_cache(cache_db);

    std::vector<DirCacheDbEntry> dirs = cache.dirs_for_kind("repo");
    int total_fail = 0;
    std::vector<std::string> repo_dirs;
    std::vector<SearchResult> results;
    CompoundResolve resolver;

    query = tea::to_lower(query);
    for (DirCacheDbEntry& cache_entry : dirs) {
        bool add_resolver = false;
        for (auto dir : DirIt(cache_entry.dir)) {
            if (!dir.is_directory())
                continue;
            std::string name = dir.path().filename().string();
            if (name.empty())
                continue;
            if (tea::to_lower(name).find(query) != std::string::npos) {
                SearchResult entry;
                entry.package_name = name;
                entry.cache_entry = cache_entry;
                results.push_back(entry);
                add_resolver = true;
            }

        }
        if (add_resolver) {
            resolver.add_finder(DirResolveDb(&string_cache, cache_entry.dir));
        }
    }

    TablePrinter table;
    table.set_header({"Library", "Latest Version", "repo"});
    for (SearchResult& result : results) {
        Dependency dep;
        dep.name = result.package_name;
        dep.constraint = VersionConstraint("+");
        std::vector<Package> packages = resolver.all_versions(dep);
        if (packages.empty())
            continue;

        for(Package& package : packages) {
            std::string repo_dir = repo_dir_from(package);
            if (repo_dir != result.cache_entry.dir)
                continue;
            std::string repo = result.cache_entry.key;
            std::string::size_type equal_pos, s_pos;
            equal_pos = repo.find('=');
            s_pos = repo.find(';');
            repo = repo.substr(equal_pos+1, s_pos - equal_pos - 1);
            table.push_back({package.name, to_string(package.version), repo});
        }
    }
    sort_list(table.mRows, [](const TablePrinter::Row& a, const TablePrinter::Row& b) -> bool {
        for (std::size_t i = 0; i < a.size(); ++i) {
            if (a[i] < b[i])
                return true;
            if (a[i] != b[i])
                return false;
        }
        return a[0] < b[0];
    });
    auto last = std::unique(table.mRows.begin(), table.mRows.end());
    table.mRows.erase(last, table.mRows.end());
    // TODO: output in json format for interation with other scripts
    // web interface will need this data
    table.print_cli();
    return total_fail > 0;
}




int main_search(std::vector<std::string> args) {
    if (args.size() <= 1) {
        print_help();
        return 1;
    }
    for(std::string& arg : args) {
        if(arg == "--help") {
            print_help();
            return 1;
        }
    }

    return search(args[1]);
}