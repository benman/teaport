#pragma once

#include <string>
#include <nlohmann/json.hpp>

#include "Sqlite3.hpp"

namespace tea {
    using csd::Sqlite3;
    using csd::Sqlite3Statement;
    std::string cache_dir();
    std::string config_dir();
    Sqlite3 init_db();
    Sqlite3 init_cache_db();
    std::string init_cache_dir();
    std::string init_http_cache_dir();

    void init_config_dir();
}