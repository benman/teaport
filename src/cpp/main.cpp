#include <string>
#include <vector>

#include "teaconfig.hpp"
#include "SafePrintf.hpp"
#include "Log.hpp"
#include "tea_dir.hpp"
#include "jsonutils.hpp"
#include "fileutils.hpp"

int main_clear_cache(std::vector<std::string> args);
int main_init(std::vector<std::string> args);
int main_install(std::vector<std::string> argsIn);
int main_lint(std::vector<std::string> args);
int main_outdated(std::vector<std::string> args);
int main_publish(std::vector<std::string> args);
int main_repo(std::vector<std::string> args);
int main_scan_urls(std::vector<std::string> args);
int main_search(std::vector<std::string> args);
int main_server(std::vector<std::string> args);
int main_status(std::vector<std::string> args);

int main_merge(std::vector<std::string> args) {
    using namespace tea;
    args.erase(args.begin());
    bool error = false;
    for (std::string& arg : args) {
        if (arg[0] == '-') {
            std::string str = "unknown argument: ";
            str += arg;
            log_message("E1019", str);
            error = true;
        }
    }
    if (error) {
        return 1;
    }
    nlohmann::json result;
    for (std::string& arg : args) {
        nlohmann::json more = load_json_file(arg);
        merge_json(result, more);
    }
    std::cout << result.dump(4) << '\n';
    return 0;
}
static void print_help() {
const char* str =
R"(teaport - downloads dependencies for your application

basic usage:
    The following will download dependencies into folder "output".

        teaport install specfile.teaspec --output-dir output/

    --version       Print version
    --help          Print this help
    -v              incrememt verbosity

Commands:
    do --help for each command to learn more. e.g. "teapot install --help"

    clear cache     delete cache files.
    init            prints a template for a project spec.
    init lib        prints a template for library/module spec.
    install         Installs dependencies. Does not update versions if lock file
                    is present.
    outdated        shows a satisfying upgrade given a spec file.
    publish         Publish a library to a repository.
    update          Like install but ignores lock file and will download newer
                    dependencies.
    repo            add/remove repos.
    lint            lint a spec file
    search          search for a library
    status          list version of some applications

)";

    printf("%s", str);
}
int main(int argc, char** argv) {
    if(argc <= 1) {
        return 1;
    }
    std::vector<std::string> args;
    std::string verbose_switch = "-v";
    int verbose_count = 1;
    for(int i = 1; i < argc; ++i) {
        if(verbose_switch == argv[i]) {
            ++verbose_count;
            tea::enable_verbose(verbose_count);
            continue;
        }
        args.push_back(argv[i]);
    }
    for(std::string& str : args) {
        if(str == "--version") {
            csd::print("teaport version {}.{}.{}", PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH);
            return 0;
        }
    }
    if(args.size() == 0 || args[0] == "--help") {
        print_help();
        return 1;
    }
    using namespace tea;
    try {
        init_config_dir();
        if(args[0] == "install" || args[0] == "update")
            return main_install(args);
        if(args[0] == "init") {
            return main_init(args);
        }
        if(args[0] == "repo")
            return main_repo(args);
        if(args[0] == "publish")
            return main_publish(args);
        if(args[0] == "lint")
            return main_lint(args);
        if(args[0] == "clear")
            return main_clear_cache(args);
        if(args[0] == "merge")
            return main_merge(args);
        if(args[0] == "status")
            return main_status(args);
        if(args[0] == "server")
            return main_server(args);
        if(args[0] == "outdated")
            return main_outdated(args);
        if(args[0] == "search")
            return main_search(args);
        if(args[0] == "scan_urls")
            return main_scan_urls(args);

    } catch(std::exception& e) {
        csd::print("exception thrown: {}", e.what());
    }
    return 1;
}