#include <exception>
#include <string>
#include <nlohmann/json.hpp>

namespace tea {
    struct LintError : std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    void lint_name(const std::string& name);
    void lint_lib_spec(const nlohmann::json& spec);
}