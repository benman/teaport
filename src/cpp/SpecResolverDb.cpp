#include "SpecResolverDb.hpp"

#include <random>


#include "SafePrintf.hpp"
#include "DirResolveDb.hpp"

using nlohmann::json;
namespace tea {
    SpecResolveDb::SpecResolveDb(StringCache* scache, ProjectSpec& projectSpec) {
        assert(scache != nullptr);
        mStringCache = scache;
        // there is no reason for this to be different for each run
        mRandomVersion = Version("ttpmdsbzsiipdvfnvwzmhjwbmtcnumgu");
        if (projectSpec.dependencies.empty())
            return;
        json& spec = projectSpec.js;
        json& deps = spec["dependencies"];

        for(json::iterator it = deps.begin(); it != deps.end(); ++it) {
            if (!it.value().is_object())
                continue;
            if (!it.value().contains("artifact")) {
                continue;
            }
            std::string name = it.key();
            int iPackage = add_package(name, it.value());
            json& dep = it.value();
            Package package = mPackages[iPackage];
            if (!dep.contains("version"))
                dep["version"] = to_string(package.version);
            if (!dep.contains("force"))
                dep["force"] = true;
            for (Dependency& projectDep : projectSpec.dependencies) {
                if (projectDep.name != name)
                    continue;
                projectDep.constraint = VersionConstraint(dep["version"]);
                projectDep.force = dep["force"];
            }
        }
    }
    SpecResolveDb::~SpecResolveDb() {

    }
    int SpecResolveDb::add_package(const std::string& name, const nlohmann::json& spec) {
        Package package;

        package = spec;
        package.js = spec;
        if (package.name.empty()) {
            package.name = name;
        }
        if (!package.artifact.tag.empty() && !package.version) {
            std::string version = "tag_";
            version += package.artifact.tag;
            for(std::size_t i = 0; i < version.size(); ++i) {
                if (std::isdigit(version[i]))
                    continue;
                if (version[i] == '-' || version[i] == '.')
                    continue;
                if (version[i] >= 'a' && version[i] <= 'z')
                    continue;
                if (version[i] >= 'A' && version[i] <= 'Z')
                    continue;
                version[i] = '_';
            }
            package.version = Version(package.artifact.tag);
        }
        if (!package.version) {
            package.version = mRandomVersion;
        }
        if (spec.contains("dev") && !spec["dev"].get<std::string>().empty()) {
            package.dev = true;
        }
        package.score = 1;
        csd::print("added embedded {}", package.name);
        mPackages.push_back(package);
        return mPackages.size()-1;
    }

    std::vector<Package> SpecResolveDb::all_versions(const Dependency &dependency) {
        assert(mStringCache != nullptr);
        for (std::size_t i = 0; i < mPackages.size(); ++i) {
            if (dependency.name != mPackages[i].name)
                continue;
            if (dependency.constraint.check(mPackages[i].version)) {
                auto packages = score_packages({mPackages[i]}, {}, *mStringCache);
                for (auto& package : packages)
                    package.score = 1;
                return packages;
            } else {
                csd::print("embedded version mismatch: {} != {}", dependency.constraint, mPackages[i].version);
            }
        }
        return {};
    }

}