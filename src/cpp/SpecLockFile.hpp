#pragma once

#include <nlohmann/json.hpp>
#include "resolve.hpp"

namespace tea {
    struct SpecLockFile {
        std::vector<Dependency> dependencies;
        int version_code;
    };

    json to_json_lock(const std::vector<Package>& resolved);
    SpecLockFile load_lock_file(std::string lock_file);

    struct SpecMetaData {
        std::vector<std::string> order;
    };
}