#include "exceptions.hpp"

#include "SafePrintf.hpp"
#include "shell.hpp"

using namespace tea;
int main() {

    try {
        throw_signal_ifneeded();
        csd::print("press ctrl+C");
        system_checked({"sleep", "1000"});
    } catch (SIGINTError& e) {
        csd::print("recieved signal");
        return 0;
    } catch(SignalError& e) {
        csd::print("recieved a signal");
        return 1;
    }
    csd::print("exiting normally...");
    return 1;
}