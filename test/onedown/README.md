this test is to test out not checking out dependencies that must not be checked
out. nolib must not be checked out because deplib 1.0.1 doesn't depend on it
but 1.0.2 does. If the output contains nolib than this test fails.