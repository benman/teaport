#pragma once

/*
    This file is intended to be the bare minimum for an extensible dependency
    resolver. So that it can be extended for a variety of purposes.
*/
#include <functional>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>


#include "Version.hpp"
#include "PackageExtras.hpp"
namespace tea {
    template< class T, class U >
    std::shared_ptr<T> reinterpret_pointer_cast( const std::shared_ptr<U>& r ) noexcept
    {
        auto p = reinterpret_cast<typename std::shared_ptr<T>::element_type*>(r.get());
        return std::shared_ptr<T>(r, p);
    }

    struct NotFoundError : std::runtime_error {
        using std::runtime_error::runtime_error;
    };



    class Dependency {
    public:
        std::string name;
        std::string dev_spec_file;
        VersionConstraint constraint;
        /** If set to true this dependency will be ignored and assumed resolved */
        bool ignore = false;
        /** If set to true this version will be used and conflicts will be ignored */
        bool force  = false;
    };


    // AKA LibrarySpec
    class Package : public PackageExtras {
    public:
        std::string name;
        Version version;
        std::vector<Dependency> dependencies;

        /** score <= 0 means package cannot be used */
        int score = 0;
    };

    class Resolve {
    public:
        virtual ~Resolve();
        virtual std::vector<Package> resolve(std::vector<Dependency> dependencies, std::vector<Dependency> locked_versions);

        /** subclasses must implement this and return all versions for a package
            of given name. Score must be set appropriatly and returned vector
            must be sorted with highest score being first element.
        */
        virtual std::vector<Package> all_versions(const Dependency &package_name);
    };

    class CompoundResolve : public Resolve {
    public:
        virtual std::vector<Package> all_versions(const Dependency &package_name);
        virtual void add_finder(const std::function<std::vector<Package>(const Dependency &)> &function);
    private:
        std::vector<std::function<std::vector<Package>(const Dependency &)>> mPackageFinders;
    };
}