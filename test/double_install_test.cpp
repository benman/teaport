#include <regex>

#include "shell.hpp"

#include "test_config.h"
#include "SafePrintf.hpp"
#include "fileutils.hpp"

using namespace tea;
int main(int argc, char** argv) {
    bool chok = tea::chdir(TEST_DIR "/onedown");
    if(!chok) {
        csd::stdout_format("could not cd into onedown\n");
        return 1;
    }
    std::string output_dir = TEST_DIR "/onedown/output";
    system_checked({TEAPORT_EXE, "update", "specs.teaspec", "--output-dir", "output"});
    if(!is_dir(output_dir)) {
        csd::stdout_format("failed to create output dir {}\n", output_dir);
        return 1;
    }
    CompletedProcess process = system_capture_checked({TEAPORT_EXE, "update", "specs.teaspec", "--output-dir", "output"});
    if(process.exit_code != 0) {
        csd::stdout_format("failed to run teaport\n");
        return 1;
    }

    // this makes sure the lockfile is read
    CompletedProcess process2 = system_capture_checked({TEAPORT_EXE, "install", "specs.teaspec", "--output-dir", "output"});
    if(process.exit_code != 0) {
        csd::stdout_format("failed to run teaport\n");
        return 1;
    }

    nlohmann::json lockfile = load_json_file(join_path(TEST_DIR, "onedown/output/meta.json"));
    std::vector<std::string> proper_order{"yeslib",
        "deplib",
        "oklib"
    };
    std::vector<std::string> order;
    for (auto& name : lockfile["order"]) {
        order.push_back(name);
    }
    if (order != proper_order) {
        csd::stdout_format("not in poper order");
        return 1;
    }

    csd::stdout_format("deleting {}\n", output_dir);
    tea::rmdir(output_dir);
    if(is_dir(output_dir)) {
        csd::stdout_format("failed to delete {}\n", output_dir);
        return 1;
    }
    process.stdout_data.push_back(0);
    process2.stdout_data.push_back(0);
    std::string str = reinterpret_cast<char*>(&process.stdout_data[0]);
    if(str.find("install") != std::string::npos) {
        csd::stdout_format("second update run did install again\n");
        return 1;
    }
    str = reinterpret_cast<char*>(&process2.stdout_data[0]);
    if(str.find("install") != std::string::npos) {
        csd::stdout_format("second install run did install again\n");
        return 1;
    }

    std::string lock = file_get_contents(join_path(TEST_DIR, "onedown/specs.teaspec.lock"));
    if(lock.find("nolib") != std::string::npos) {
        csd::stdout_format("no lib should not have been resolved\n");
        return 1;
    }

    return 0;
}