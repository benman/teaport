#include "tea_dir.hpp"

#include <nlohmann/json.hpp>

#include "fileutils.hpp"

static const char* init_sql = R"(
CREATE TABLE IF NOT EXISTS repos (
    id              INTEGER PRIMARY KEY,
    name            TEXT,
    path            TEXT,
    url             TEXT,
    last_updated    DATETIME
);
)";
namespace tea {
    static const char* kcache_dir = nullptr;
    std::string cache_dir()  { 
        if(kcache_dir)
            return kcache_dir;
        return home_dir() + "/.cache/teaport";
    }
    std::string config_dir() { return home_dir() + "/.config/teaport";  }

    void init_config_dir() {
        if(!is_dir(config_dir())) {
            mkdir(config_dir());
        }
        std::string config_file = join_path(config_dir(), "teaport.json");
        if(is_file(config_file)) {
            nlohmann::json config = load_json_file(config_file);
            if(config.find("cache_dir") != config.end()) {
                std::string path = config["cache_dir"];
                path = expand_path(path);
                kcache_dir = strdup(path.c_str());
            }
        }
    }
    Sqlite3 init_db() {
        init_config_dir();
        std::string db_file = config_dir() + "/teaport.db";
        Sqlite3 database(db_file);
        database.exec(init_sql);

        return std::move(database);
    }


    Sqlite3 init_cache_db() {
        std::string cache_dir = tea::cache_dir();
        mkdir(cache_dir);
        assert(is_dir(cache_dir));
        std::string db_file = cache_dir + "/db.db";
        Sqlite3 db;
        db.open(db_file);
        return std::move(db);
    }

    std::string init_subcache_dir(std::string sub) {
        std::string cache_dir = tea::cache_dir();
        mkdir(cache_dir);
        assert(is_dir(cache_dir));
        cache_dir += "/";
        cache_dir += sub;
        mkdir(cache_dir);
        assert(is_dir(cache_dir));
        return cache_dir;
    }

    std::string init_cache_dir() {
        return init_subcache_dir("artifacts");
    }

    std::string init_http_cache_dir() {
        return init_subcache_dir("http");
    }
}