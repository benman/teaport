#include "Sqlite3.hpp"
#include "fileutils.hpp"
#include "tea_dir.hpp"
#include "shell.hpp"
#include <nlohmann/json.hpp>
#include "resolve.hpp"
#include "DirCache.hpp"
#include "Log.hpp"
#include "stringutils.hpp"
#include "git.hpp"

using namespace tea;
static int print_help() {
const char* str=
R"(teaport repo - manipulate repo information
    update <spec-file>
        update all repos in spec file

    add <repo-name> <url>
        add a new repo.

    remove <repo-name>
        removes the specified repo.

    list
        List all repos
)";
    printf("%s", str);
    return 1;
}

static int update_repo(Sqlite3& db, std::string name) {
    Sqlite3Statement statement = db.prepare("SELECT path, url FROM repos WHERE name=?");
    statement.bind_text(1, name);
    while(statement.step() == SQLITE_ROW) {
        std::string path    = statement.column_text(0);
        std::string url     = statement.column_text(1);
        if(!path_exists(path)) {
            int result = tea::system({"git", "clone", "--depth", "1", "--single-branch", url, path});
            if(result != 0)
                return 1;
            break;
        }
        int result = tea::system({"git", "-C", path, "pull", "--depth", "1", "--single-branch,", url});
        if(result != 0)
            return 1;
    }
    statement = db.prepare("UPDATE repos SET last_updated = strftime('%s','now') WHERE name=?");
    statement.bind_text(1, name);
    return statement.step() == SQLITE_DONE;

}

static int update_all() {
    Sqlite3 cache_db = init_cache_db();
    DirCache cache(cache_db, init_cache_dir());

    std::vector<DirCacheDbEntry> dirs = cache.dirs_for_kind("repo");
    int total_fail = 0;
    for (DirCacheDbEntry& dir : dirs) {
        if (starts_with(dir.key, "git_url=")) {
            total_fail += git_make_latest(dir.dir, dir.key)? 0: 1;
        } else {
            // Next time this repo will be updated. Unfortunatley no searching
            // while deleted :P.
            tea::rmdir(dir.dir);
            // TODO: redownload the repo so user can continue to search it.
        }
    }
    return total_fail > 0;
}

// teaport repo update <spec-file>
static int update(std::vector<std::string> args) {
    if(args.size() < 2) {
        print_help();
        return 1;
    }
    if (args.size() < 3)
        return update_all();
    std::string spec_file = args[2];
    nlohmann::json spec_js = load_json_file(spec_file);
    nlohmann::json repos = spec_js["repos"];
    std::string artifact_cache_dir = init_cache_dir();
    Package spec = spec_js;

    Sqlite3 database = init_cache_db();
    DirCache cache(database, artifact_cache_dir);

    for(nlohmann::json repo : repos) {
        if(repo.find("git") == repo.end())
            continue;
        std::string git_url = repo["git"];
        std::string dir = cache.dir_for_key(git_url);
        if(is_dir(dir)) {
            git_make_latest(dir, git_url);
        }
    }

    return log_error_count();

}

// teaport repo add <name> <url>
static int add(std::vector<std::string> args) {
    if(args.size() < 4) {
        print_help();
        return 1;
    }
    std::string name = args[2];
    std::string url = args[3];
    std::string path = config_dir() + "/repos";
    mkdir(path);
    path += "/";
    path += name;
    if(path_exists(path))
        tea::rmdir(path);
    int result = tea::system({"git", "clone", "--depth", "1", "--single-branch", url, path});
    if(result != 0)
        return 1;
    Sqlite3 db = init_db();
    Sqlite3Statement statement = db.prepare("INSERT INTO REPOS (name, path, url, last_updated) VALUES (?, ?, ?, strftime('%s','now'))");
    statement.bind_text(1, name);
    statement.bind_text(2, path);
    statement.bind_text(3, url);
    int success = statement.step();
    if(success == SQLITE_OK)
        return 0;
    return 1;
}

// teaport repo remove <name>
static int remove(std::vector<std::string> args) {
    if(args.size() < 3) {
        print_help();
        return 1;
    }
    std::string name = args[2];
    Sqlite3 db = init_db();
    Sqlite3Statement statement = db.prepare("SELECT path FROM repos WHERE name=?");
    statement.bind_text(1, name);

    while(statement.step() == SQLITE_ROW) {
        nlohmann::json repo;
        std::string path = statement.column_text(0);
        tea::rmdir(path);
    }
    statement = db.prepare("DELETE FROM repos WHERE name=?");
    statement.bind_text(1, name);
    statement.step();
    return 0;
}

static int list(std::vector<std::string> args) {
    Sqlite3 db = init_db();
    Sqlite3Statement statement = db.prepare("SELECT name, path, url, last_updated FROM repos");
    nlohmann::json repo_list;

    while(statement.step() == SQLITE_ROW) {
        nlohmann::json repo;
        repo["name"]            = statement.column_text(0);
        repo["path"]            = statement.column_text(1);
        repo["url"]             = statement.column_text(2);
        repo["last_updated"]    = statement.column_text(3);
        repo_list.push_back(repo);
    }
    Sqlite3 cache_db = init_cache_db();
    DirCache cache(cache_db, init_cache_dir());

    std::vector<DirCacheDbEntry> cached_repos = cache.dirs_for_kind("repo");
    for (DirCacheDbEntry& cached_repo : cached_repos) {
        nlohmann::json repo;
        repo["path"] = cached_repo.dir;
        repo["url"] = cached_repo.key;
        repo_list.push_back(repo);
    }
    csd::print("{}", repo_list.dump(4));
    return 0;
}


int main_repo(std::vector<std::string> args) {
    if (args.size() <= 1) {
        print_help();
        return 1;
    }
    for(std::string& arg : args) {
        if(arg == "--help") {
            print_help();
            return 1;
        }
    }
    if (args.empty()) {
        return list(args);
    } else if(args[1] == "update") {
        return update(args);
    } else if(args[1] == "add") {
        return add(args);
    } else if(args[1] == "remove") {
        return remove(args);
    } else if(args[1] == "list") {
        return list(args);
    }

    print_help();
    return 1;
}