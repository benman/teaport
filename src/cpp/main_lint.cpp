#include <string>
#include <nlohmann/json.hpp>

#include "fileutils.hpp"
#include "lint.hpp"
#include "SafePrintf.hpp"

using nlohmann::json;
using namespace tea;
using namespace csd;

static int print_help(){
const char* str =
R"(
    lint <spec>
        lint the spec file. Exits 0 if things look ok

)";

    printf("%s", str);
    return 1;
}


int main_lint(std::vector<std::string> args) {
    args.erase(args.begin());
    if(args.size() < 1)
        return print_help();
    for(std::string& arg : args) {
        if(arg == "--help") {
            return print_help();
        }
    }

    json js = tea::load_json_file(args[0]);
    try {
        lint_lib_spec(js);
    } catch(LintError& e) {
        print("lint error: {}", e.what());
        return 1;
    }
    return 0;
}