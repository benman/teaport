#pragma once

#include <nlohmann/json.hpp>
#include "resolve.hpp"
#include "resolve.hpp"
#include "ProjectSpec.hpp"

namespace tea {
    class SpecResolveDb {
    public:
        SpecResolveDb(StringCache* scache, ProjectSpec& spec);
        ~SpecResolveDb();

        std::vector<Package> all_versions(const Dependency &dep);
        std::vector<Package> operator()(const Dependency &dep) {
            return all_versions(dep);
        }
    private:
        /** @returns index in mPackages for new package */
        int add_package(const std::string& name, const nlohmann::json& spec);
        std::vector<Package> mPackages;

        // the version all packages are assigned
        Version mRandomVersion;
        StringCache* mStringCache;
    };
}