#include <string>
#include <nlohmann/json.hpp>

#include "TeaspecInstaller.hpp"
#include "fileutils.hpp"
#include "lint.hpp"
#include "SafePrintf.hpp"
#include "TablePrinter.hpp"

using nlohmann::json;
using namespace tea;
using namespace csd;

static int print_help(){
const char* str =
R"(
    outdated <spec>
        lists new versions of libraries listed as dependencies in spec file.

)";

    printf("%s", str);
    return 1;
}

struct Row {
    std::string package_name;
    Version     current_version;
    Version     restricted_version;
    Version     latest_version;
};

// TODO: needs a test
int main_outdated(std::vector<std::string> args) {
    args.erase(args.begin());
    if(args.size() < 1)
        return print_help();
    for(std::string& arg : args) {
        if(arg == "--help") {
            return print_help();
        }
    }

    ProjectSpec spec = tea::load_project_spec(args[0]);
    TeaspecInstaller installer(spec);
    installer.resolve();

    std::map<std::string, Row> checked_version;

    for (Package& package : installer.resolved) {
        Row row;
        row.package_name = package.name;
        row.restricted_version = package.version;
        checked_version[package.name] = row;
    }


    for (Dependency& dependency : spec.dependencies) {
        dependency.constraint = VersionConstraint("+");
    }

    installer = TeaspecInstaller(spec);
    installer.resolve();

    installer.load_lock_file();
    TablePrinter table;

    table.set_header({"Library", "Version", "update", "latest"});
    std::map<std::string, bool> specified_deps;
    for (Dependency& dep : spec.dependencies) {
        specified_deps[dep.name] = true;
    }

    for (Package& package : installer.resolved) {
        Row& row = checked_version[package.name];
        row.latest_version = package.version;
    }

    for (Dependency& dep : installer.locked_versions) {
        Row& row = checked_version[dep.name];
        row.current_version = dep.constraint.min_version;
        checked_version[dep.name] = row;
    }

    for (auto& pair : checked_version) {
        pair.second.package_name = pair.first;
    }

    for(auto& pair : checked_version) {
        Row version_info = pair.second;
        // don't show latest
        if (version_info.current_version == version_info.latest_version)
            continue;
        TablePrinter::Row row{version_info.package_name,
            to_string(version_info.current_version),
            to_string(version_info.restricted_version),
            to_string(version_info.latest_version)};
        table.push_back(row);
    }
    table.print_cli();
    return 0;
}