This page is just so you can tap into my thoughts to have a better understanding
of the decisions made. Bottom line I want something simple, and unfortunately
that means not supporting complex items. But where I draw the line is arbitrary.

# project generators? yes no?

This project I want to be a pure dependency manager that applications can build
on top on. If this is too general then it will be useless as it will have too
much configurations. If it's too simple then it will be useless as it won't have
features needed. The key is a balance, which is hard. And I don't know if project
generators is a good idea at this level. Or to help with a way to have project
generators at this level. Perhaps anather tool should take on the responsibility
for integrating dependencies into a project.

Maybe using a scripting embedded scripting language it will assist in making
integregrated tools. But it's easier for you to just write a wrapper in your
choice of language and simply invoke the tool. Like a plugin to simply extract
dependencies for your custom build. But in that case you can just generate
a teaspec file and pass it in to teaport.

# cons

- every dependency needs a teaspec file
- because of all the variety of compilers, platforms, & switches I think the
  best way is to distribute sources and have ccache to speed up compilation.
- c++ has way too many build systems. Specializing for any build system will
  alienate users that don't use that build system. Not specializing for any
  build system equally alienates everyone.
- Most c++ libraries have various complexities in compiling.
- no support for optional dependencies
